﻿using SaintGobain.ImpresionDeEtiquetas.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;

namespace SaintGobain.ImpresionDeEtiquetas.Repository
{
    public class Impresion

    {
        private readonly SQLManager sqlManager;

        public Impresion(string CadenaDeConexionSQL)
        {
            sqlManager = new SQLManager(CadenaDeConexionSQL);
        }

        public void ImprimirEtiquetasNoProcesadas()
        {
            var listaEtiquetasAImprimir = sqlManager.GetEtiquetasAImprimir();
            if(listaEtiquetasAImprimir.Count > 0)
            {
                Logger.LogTitle("--------------------- Inicio de Impresión de Etiquetas No Procesadas ---------------------");
            }
            else
            {
                return;
            }

            var listaEtiquetasUnicasAImprimir = ObtenerEtiquetasUnicas(listaEtiquetasAImprimir);
            Logger.LogInfo($"Cantidad de etiquetas a imprimir: {listaEtiquetasUnicasAImprimir.Count}.");
            if(listaEtiquetasUnicasAImprimir.Count > 0)
            {
                Logger.LogInfo("Imprimiendo etiquetas.");
                var listaEtiquetasImpresas = ImprimirEtiquetas(listaEtiquetasUnicasAImprimir);
                bool etiquetasActualizadasEnBaseDeDatos = sqlManager.ActualizarEtiquetasProcesadas(listaEtiquetasImpresas);
                if (etiquetasActualizadasEnBaseDeDatos == true) Logger.LogInfo("Etiquetas actualizadas en Base de Datos.");
            }
            else
            {
                Logger.LogInfo("No se encontraron etiquetas únicas para imprimir.");
            }

            Logger.LogTitle("--------------------- Fin de Impresión de Etiquetas No Procesadas ---------------------");
        }


        public List<EtiquetaAImprimir> ImprimirEtiquetas(List<EtiquetaAImprimir> ListaEtiquetasAImprimir)
        {
            var listaEtiquetasImpresas = new List<EtiquetaAImprimir>();

            foreach (var etiquetaAImprimir in ListaEtiquetasAImprimir)
            {
                var etiquetaImpresa = ImprimirEtiqueta(etiquetaAImprimir);
                if (etiquetaImpresa != null) listaEtiquetasImpresas.Add(etiquetaImpresa);
            }

            return listaEtiquetasImpresas;
        }
        public EtiquetaAImprimir ImprimirEtiqueta(EtiquetaAImprimir EtiquetaAImprimir)
        {
            EtiquetaAImprimir etiquetaImpresa = null;
            string logInfoEtiqueta = $"(MensajeRolloId: {EtiquetaAImprimir.MensajeRolloId}, ImpresoraNombre: {EtiquetaAImprimir.ImpresoraNombre}, UnidadProductivaNombre: {EtiquetaAImprimir.UnidadProductivaNombre}, ArticuloCodigo: {EtiquetaAImprimir.ArticuloCodigo})";
            var tipoDeImpresora = (TipoDeImpresora)EtiquetaAImprimir.TipoImpresoraId;
            bool impresionExitosa = false;
            switch (tipoDeImpresora)
            {
                case TipoDeImpresora.Zebra:
                    var impresoraZebra = new ImpresoraZebra(EtiquetaAImprimir.ImpresoraHostname, EtiquetaAImprimir.ImpresoraPuerto);
                    impresionExitosa = impresoraZebra.ImprimirEtiqueta(EtiquetaAImprimir);
                    break;
                case TipoDeImpresora.MarkemImaje:
                    var impresoraMarkemImaje = new ImpresoraMarkemImaje(EtiquetaAImprimir.ImpresoraHostname, EtiquetaAImprimir.ImpresoraPuerto);
                    impresionExitosa = impresoraMarkemImaje.ImprimirEtiqueta(EtiquetaAImprimir);
                    break;
                default:
                    break;
            }

            if (impresionExitosa == true)
            {
                etiquetaImpresa = EtiquetaAImprimir;
                etiquetaImpresa.EtiquetaProcesada = true;
                Logger.LogInfo($"Etiqueta impresa correctamente {logInfoEtiqueta}.");
            }
            else
            {
                Logger.LogWarn($"No se pudo imprimir etiqueta {logInfoEtiqueta}.");
            }

            return etiquetaImpresa;
        }

        private List<EtiquetaAImprimir> ObtenerEtiquetasUnicas(List<EtiquetaAImprimir> ListaEtiquetasAImprimir)
        {
            var listaEtiquetasAImprimirUnicas = new List<EtiquetaAImprimir>();
            if (ListaEtiquetasAImprimir != null)
            {
                List<int> listaMensajeRolloID = ListaEtiquetasAImprimir.Select(c => c.MensajeRolloId).ToList();
                foreach (var mensajeRolloID in listaMensajeRolloID)
                {
                    var etiquetaAImprimirSegunId = ListaEtiquetasAImprimir.Where(c => c.MensajeRolloId == mensajeRolloID).ToList();
                    if (etiquetaAImprimirSegunId.Count() == 1)
                    {
                        listaEtiquetasAImprimirUnicas.AddRange(etiquetaAImprimirSegunId);
                    }
                    else
                    {
                        Logger.LogWarn($"Se obtuvo más de una entrada para un mismo Id de rollo (MensajeRolloId = {mensajeRolloID}).");
                    }
                }
            }

            return listaEtiquetasAImprimirUnicas;
        }

    }
}
