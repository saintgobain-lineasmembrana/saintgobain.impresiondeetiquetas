﻿using SaintGobain.ImpresionDeEtiquetas.Common;
using System.Collections.Generic;
using System.Linq;

namespace SaintGobain.ImpresionDeEtiquetas.Repository
{
    public class ImpresoraMarkemImaje
    {
        private readonly string hostname;
        private readonly int puertoTCP;
        private string comandoObtenerEstadoImpresoraListaParaImprimir;
        private string comandoEjecutarImpresion;
        private string respuestaEsperadaEstadoImpresoraListaParaImprimir;
        private string respuestaEsperadaEjecutarImpresion;
        private string respuestaEsperadaSeleccionarTemplate;
        private string respuestaEsperadaActualizarTemplate;

        public ImpresoraMarkemImaje(string Hostname, int PuertoTCP)
        {
            hostname = Hostname;
            puertoTCP = PuertoTCP;
            AsignarValoresAComandosYRespuestasEsperadas("~PS|1|", "~PG|0|1|", "~PS0|", "~PG0|", "~JS0|", "~JU0|");
        }

        public ImpresoraMarkemImaje(string Hostname, int PuertoTCP, string ComandoObtenerEstadoImpresoraListaParaImprimir, string ComandoEjecutarImpresion, string RespuestaEsperadaEstadoImpresoraListaParaImprimir, string RespuestaEsperadaEjecutarImpresion, string RespuestaEsperadaSeleccionarTemplate, string RespuestaEsperadaActualizarTemplate)
        {
            hostname = Hostname;
            puertoTCP = PuertoTCP;
            AsignarValoresAComandosYRespuestasEsperadas(ComandoObtenerEstadoImpresoraListaParaImprimir, ComandoEjecutarImpresion, RespuestaEsperadaEstadoImpresoraListaParaImprimir, RespuestaEsperadaEjecutarImpresion, RespuestaEsperadaSeleccionarTemplate, RespuestaEsperadaActualizarTemplate);
        }

        private void AsignarValoresAComandosYRespuestasEsperadas(string ComandoObtenerEstadoImpresoraListaParaImprimir, string ComandoEjecutarImpresion, string RespuestaEsperadaEstadoImpresoraListaParaImprimir, string RespuestaEsperadaEjecutarImpresion, string RespuestaEsperadaSeleccionarTemplate, string RespuestaEsperadaActualizarTemplate)
        {
            comandoObtenerEstadoImpresoraListaParaImprimir = ComandoObtenerEstadoImpresoraListaParaImprimir;
            comandoEjecutarImpresion = ComandoEjecutarImpresion;
            respuestaEsperadaEstadoImpresoraListaParaImprimir = RespuestaEsperadaEstadoImpresoraListaParaImprimir;
            respuestaEsperadaEjecutarImpresion = RespuestaEsperadaEjecutarImpresion;
            respuestaEsperadaSeleccionarTemplate = RespuestaEsperadaSeleccionarTemplate;
            respuestaEsperadaActualizarTemplate = RespuestaEsperadaActualizarTemplate;
        }

        public bool ImprimirEtiqueta(EtiquetaAImprimir EtiquetaAImprimir)
        {
            bool impresionExitosa = false;
            Logger.LogDebug("Evaluando si impresora está lista para imprimir.");
            bool impresoraListaParaImprimir = EnviarPeticionAImpresora(PeticionesMarkemImaje.EstadoImpresoraListaParaImprimir);
            if (impresoraListaParaImprimir == true)
            {
                Logger.LogDebug("Seleccionando y actualizando Job de impresora.");
                string templateCSVConValores = TemplateManager.AsignarValoresATemplate(EtiquetaAImprimir);
                bool templateActualizado = SeleccionarYActualizarJobEnImpresora(templateCSVConValores);
                if (templateActualizado == true) 
                {
                    Logger.LogDebug("Enviando comando de impresión.");
                    bool impresionEjecutada = EnviarPeticionAImpresora(PeticionesMarkemImaje.EjecutarImpresion);
                    if (impresionEjecutada == true) impresionExitosa = true; Logger.LogDebug("Impresión exitosa.");
                }
            }

            return impresionExitosa;
        }

        public bool EnviarPeticionAImpresora(PeticionesMarkemImaje EstadoMarkemImaje, List<string> ListaParametrosAEnviar = null)
        {
            bool peticionEjecutadaExitosamente = false;
            (string comandoAEnviar, string respuestaEsperada) = ObtenerComandoYRespuestaEsperadaParaPeticion(EstadoMarkemImaje, ListaParametrosAEnviar);
            if (comandoAEnviar != "" && respuestaEsperada != "")
            {
                Logger.LogDebug($"ComandoAEnviar = {comandoAEnviar}");
                string respuestaRecibida = EnviarDataYRecibirRespuestaDeImpresora(comandoAEnviar);
                Logger.LogDebug($"RespuestaRecibida = {respuestaRecibida}, RespuestaEsperada = {respuestaEsperada}");
                if (respuestaRecibida == respuestaEsperada) peticionEjecutadaExitosamente = true;
            }

            return peticionEjecutadaExitosamente;
        }

        public string EnviarDataYRecibirRespuestaDeImpresora(string ComandoAEnviar)
        {
            var tcpManager = new TCPManager(hostname, puertoTCP);
            string respuestaRecibida = tcpManager.EnviarDataYRecibirRespuesta(ComandoAEnviar);
            tcpManager.CerrarConexion();

            return respuestaRecibida;
        }

        private (string ComandoAEnviar, string RespuestaEsperada) ObtenerComandoYRespuestaEsperadaParaPeticion(PeticionesMarkemImaje EstadoMarkemImaje, List<string> ListaParametrosAEnviar = null)
        {
            string comandoAEnviar = "";
            string respuestaEsperada = "";
            switch (EstadoMarkemImaje)
            {
                case PeticionesMarkemImaje.EstadoImpresoraListaParaImprimir:
                    comandoAEnviar = comandoObtenerEstadoImpresoraListaParaImprimir;
                    respuestaEsperada = respuestaEsperadaEstadoImpresoraListaParaImprimir;
                    break;
                case PeticionesMarkemImaje.EjecutarImpresion:
                    comandoAEnviar = comandoEjecutarImpresion;
                    respuestaEsperada = respuestaEsperadaEjecutarImpresion;
                    break;
                case PeticionesMarkemImaje.SeleccionarJob:
                    if(ListaParametrosAEnviar != null)
                    {
                        comandoAEnviar = GenerarComandoParaSeleccionarJobEnImpresora(ListaParametrosAEnviar);
                        respuestaEsperada = respuestaEsperadaSeleccionarTemplate;
                    }
                    else
                    {
                        Logger.LogWarn("No se recibió data adicional a enviar para la petición \"SeleccionarJob\".");
                    }
                    break;
                case PeticionesMarkemImaje.ActualizarJob:
                    if(ListaParametrosAEnviar != null)
                    {
                        comandoAEnviar = GenerarComandoParaActualizarJobEnImpresora(ListaParametrosAEnviar);
                        respuestaEsperada = respuestaEsperadaActualizarTemplate;
                    }
                    else
                    {
                        Logger.LogWarn("No se recibió data adicional a enviar para la petición \"ActualizarJob\".");
                    }
                    break;
                default:
                    break;
            }

            return (comandoAEnviar, respuestaEsperada);
        }

        private bool SeleccionarYActualizarJobEnImpresora(string TemplateCSV)
        {
            bool templateSeleccionadoYActualizado = false;
            List<string> listaParametros = TemplateManager.ObtenerValoresDeTemplateCSV(TemplateCSV);
            if (listaParametros.Count > 0)
            {
                bool templateSeleccionado = EnviarPeticionAImpresora(PeticionesMarkemImaje.SeleccionarJob, listaParametros);
                if (templateSeleccionado == true)
                {
                    if (listaParametros.Count > 1)
                    {
                        bool templateActualizado = EnviarPeticionAImpresora(PeticionesMarkemImaje.ActualizarJob, listaParametros);
                        if (templateActualizado == true) templateSeleccionadoYActualizado = true;
                    }
                    else
                    {
                        templateSeleccionadoYActualizado = true;
                    }
                }
            }

            return templateSeleccionadoYActualizado;
        }

        private string GenerarComandoParaSeleccionarJobEnImpresora(List<string> ListaParametrosAEnviar)
        {
            string comandoParaSeleccionarTemplate = "";
            if(ListaParametrosAEnviar.Count > 0)
            {
                comandoParaSeleccionarTemplate = $"\u0002JS|{ListaParametrosAEnviar[0]}|0|\u0003";
                Logger.LogDebug($"Comando generado: {comandoParaSeleccionarTemplate}");
            }

            return comandoParaSeleccionarTemplate;
        }

        private string GenerarComandoParaActualizarJobEnImpresora(List<string> ListaParametrosAEnviar)
        {
            string comandoParaActualizarTemplate = "";
            if (ListaParametrosAEnviar.Count > 0)
            {
                bool agregarAlocacion = true;
                foreach(var parametroAEnviar in ListaParametrosAEnviar)
                {
                    comandoParaActualizarTemplate +=  parametroAEnviar + '|';
                    if (agregarAlocacion == true) comandoParaActualizarTemplate += "0|"; agregarAlocacion = false;
                }
                comandoParaActualizarTemplate = $"\u0002JU|{comandoParaActualizarTemplate}0|\u0003";
                Logger.LogDebug($"Comando generado: {comandoParaActualizarTemplate}");
            }

            return comandoParaActualizarTemplate;
        }
    }
}
