﻿using SaintGobain.ImpresionDeEtiquetas.Common;
using System;

namespace SaintGobain.ImpresionDeEtiquetas.Repository
{
    public class ImpresoraZebra
    {
        private readonly string hostname;
        private readonly int puertoTCP;

        public ImpresoraZebra(string Hostname, int PuertoTCP)
        {
            hostname = Hostname;
            puertoTCP = PuertoTCP;
        }

        public bool ImprimirEtiqueta(EtiquetaAImprimir EtiquetaAImprimir)
        {
            string templateZPLConValores = TemplateManager.AsignarValoresATemplate(EtiquetaAImprimir);
            var tcpManager = new TCPManager(hostname, puertoTCP);

            bool dataEnviada = tcpManager.EnviarData(templateZPLConValores);
            tcpManager.CerrarConexion();

            return dataEnviada;
        }
    }
}
