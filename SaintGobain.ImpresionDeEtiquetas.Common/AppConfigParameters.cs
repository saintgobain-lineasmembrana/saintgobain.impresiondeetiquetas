﻿using System.Collections.Specialized;
using System.Configuration;

namespace SaintGobain.ImpresionDeEtiquetas.Common.AppConfig
{
    public class AppConfigParameters : IParameters
    {
        private const string mensajeParametroNoEncontrado = "Parámetro \"{0}\" no encontrado en archivo .config.";

        public bool GetBool(string NombreDelParametro, bool ValorDefault = false)
        {
            string parametroEnAppConfig = ObtenerParametroEnAppConfig(NombreDelParametro);
            bool parametroBool = ValorDefault;

            if(parametroEnAppConfig != null)
            {
                if (parametroEnAppConfig.ToLower() == "true")
                {
                    parametroBool = true;
                }
            }
            else
            {
                Logger.LogWarn(string.Format(mensajeParametroNoEncontrado, NombreDelParametro));
            }

            return parametroBool;
        }

        public int GetInt(string NombreDelParametro, int ValorDefault = 0)
        {
            string parametroEnAppConfig = ObtenerParametroEnAppConfig(NombreDelParametro);
            int parametroInt = ValorDefault;

            if (parametroEnAppConfig != null)
            {
                bool parseExitoso = int.TryParse(parametroEnAppConfig, out int resultadoParse);
                if (parseExitoso == true)
                {
                    parametroInt = resultadoParse;
                }
            }

            return parametroInt;
        }

        public double GetDouble(string NombreDelParametro, int ValorDefault = 0)
        {
            string parametroEnAppConfig = ObtenerParametroEnAppConfig(NombreDelParametro);
            double parametroDouble = ValorDefault;

            if (parametroEnAppConfig != null)
            {
                bool parseExitoso = double.TryParse(parametroEnAppConfig, out double resultadoParse);
                if (parseExitoso == true)
                {
                    parametroDouble = resultadoParse;
                }
            }

            return parametroDouble;
        }

        public string GetString(string NombreDelParametro, string ValorDefault = "")
        {
            string parametroEnAppConfig = ObtenerParametroEnAppConfig(NombreDelParametro);
            string parametroString = ValorDefault;

            if (parametroEnAppConfig != null)
            {
                parametroString = parametroEnAppConfig;
            }
            else
            {
                Logger.LogWarn(string.Format(mensajeParametroNoEncontrado, NombreDelParametro));
            }

            return parametroString;
        }

        private string ObtenerParametroEnAppConfig(string NombreDelParametro)
        {
            NameValueCollection appSettings = null;
            string parametroEnAppConfig = null;

            try
            {
                appSettings = ConfigurationManager.AppSettings;
            }
            catch (ConfigurationErrorsException e)
            {
                Logger.LogError($"Error al obtener \"AppSettings\":\n{e}");
            }

            if(appSettings != null)
            {
                parametroEnAppConfig = appSettings[NombreDelParametro];
            }

            return parametroEnAppConfig;
        }
    }
}
