﻿namespace SaintGobain.ImpresionDeEtiquetas.Common
{
    public enum TipoDeImpresora
    {
        Zebra = 1,
        MarkemImaje = 2
    }

    public enum ComunicacionConImpresora
    {
        TCPIP = 1,
        HTTP = 2
    }

    public enum PeticionesMarkemImaje
    {
        EstadoImpresoraListaParaImprimir = 1,
        EjecutarImpresion = 2,
        SeleccionarJob = 3,
        ActualizarJob = 4
    }


    public enum EstadoEtiqueta
    {
        NoProcesada = 1,
        NoImpresa = 2,
        Impresa = 3
    }
}
