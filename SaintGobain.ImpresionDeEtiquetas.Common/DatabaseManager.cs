﻿using SaintGobain.ImpresionDeEtiquetas.Common.SGPv2_SGModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace SaintGobain.ImpresionDeEtiquetas.Common
{
    public class DatabaseManager
    {
        public List<EtiquetaAImprimir> GetEtiquetasAImprimir()
        {
            List<EtiquetaAImprimir> listaEtiquetasAImprimir = EjecutarStoredProcedure<EtiquetaAImprimir>("GetEtiquetasAImprimir");
            var listaPropiedadesDeEtiqueta = new List<PropiedadesDeEtiqueta>();
            if(listaEtiquetasAImprimir.Count > 0)
            {
                try
                {
                    List<List<string>> listaPropiedadesDeEtiquetaEnString = ObtenerValoresDeTablaDinamica("GetTablaArticulosPropiedades");
                    foreach (var propiedadDeEtiquetaEnString in listaPropiedadesDeEtiquetaEnString)
                    {
                        var propiedadDeEtiqueta = new PropiedadesDeEtiqueta();
                        for (var i = 0; i < propiedadDeEtiquetaEnString.Count; i++)
                        {
                            switch (propiedadDeEtiquetaEnString[i])
                            {
                                case "Articulo":
                                    propiedadDeEtiqueta.ArticuloNombre = propiedadDeEtiquetaEnString[i + 1];
                                    break;
                                case "LPMCodigoQRBase64":
                                    propiedadDeEtiqueta.CodigoQRBase64 = propiedadDeEtiquetaEnString[i + 1];
                                    break;
                                case "LPMTextoCuerpoLinea01":
                                    propiedadDeEtiqueta.TextoCuerpoLinea01 = propiedadDeEtiquetaEnString[i + 1];
                                    break;
                                case "LPMTextoCuerpoLinea02":
                                    propiedadDeEtiqueta.TextoCuerpoLinea02 = propiedadDeEtiquetaEnString[i + 1];
                                    break;
                                case "LPMTextoCuerpoLinea03":
                                    propiedadDeEtiqueta.TextoCuerpoLinea03 = propiedadDeEtiquetaEnString[i + 1];
                                    break;
                                case "LPMTextoCuerpoLinea04":
                                    propiedadDeEtiqueta.TextoCuerpoLinea04 = propiedadDeEtiquetaEnString[i + 1];
                                    break;
                                case "LPMTextoPieDeEtiqueta":
                                    propiedadDeEtiqueta.TextoPieDeEtiqueta = propiedadDeEtiquetaEnString[i + 1];
                                    break;

                                // Agregado por Ignacio Lacioppa 11/08/2022
                                case "LPMTamañoTextoCuerpoLinea01":
                                    propiedadDeEtiqueta.TamañoTextoCuerpoLinea01 = propiedadDeEtiquetaEnString[i + 1];
                                    break;
                                case "LPMTamañoTextoCuerpoLinea02":
                                    propiedadDeEtiqueta.TamañoTextoCuerpoLinea02 = propiedadDeEtiquetaEnString[i + 1];
                                    break;
                                case "LPMTamañoTextoCuerpoLinea03":
                                    propiedadDeEtiqueta.TamañoTextoCuerpoLinea03 = propiedadDeEtiquetaEnString[i + 1];
                                    break;
                                case "LPMTamañoTextoCuerpoLinea04":
                                    propiedadDeEtiqueta.TamañoTextoCuerpoLinea04 = propiedadDeEtiquetaEnString[i + 1];
                                    break;
                                case "LPMTamañoTextoPieDeEtiqueta":
                                    propiedadDeEtiqueta.TamañoTextoPieDeEtiqueta = propiedadDeEtiquetaEnString[i + 1];
                                    break;
                                default:
                                    break;
                            }
                        }
                        if (propiedadDeEtiqueta.ArticuloNombre != null && propiedadDeEtiqueta.ArticuloNombre != "") listaPropiedadesDeEtiqueta.Add(propiedadDeEtiqueta);
                    }
                }
                catch (Exception e)
                {
                    Logger.LogError($"Error al obtener propiedades de artículos:\n{e}");
                }
                listaEtiquetasAImprimir.ForEach(c => c.PropiedadesDeEtiqueta = listaPropiedadesDeEtiqueta.FirstOrDefault(d => d.ArticuloNombre == c.ArticuloNombre));
                listaEtiquetasAImprimir.ForEach(c => c.TemplateAUtilizar = TemplateManager.ObtenerTemplateAUtilizar(c));
                listaEtiquetasAImprimir.OrderByDescending(c => c.MensajeRolloId);
            }
            return listaEtiquetasAImprimir;
        }

        public int GetEstadoEtiquetaDeRolloAnterior(string LoteNumero, int? RolloNumero)
        {
            int estadoEtiquetaId = (int)EstadoEtiqueta.NoProcesada;
            using (var dbEntities = new SGPv2_SGEntities())
            {
                try
                {
                    var listaMensajeRollo = dbEntities.SG_MensajeRollo.Where(c => c.LoteNumero == LoteNumero).OrderByDescending(c => c.Id).ToList();
                    var mensajeRolloActual = listaMensajeRollo.SingleOrDefault(c => c.RolloNumero == RolloNumero);
                    if(mensajeRolloActual != null)
                    {
                        var idNumeroRolloActual = mensajeRolloActual.Id;
                        var mensajeRolloAnterior = listaMensajeRollo.Where(c => c.Id < idNumeroRolloActual).FirstOrDefault();
                        if (mensajeRolloAnterior != null)
                        {
                            estadoEtiquetaId = mensajeRolloAnterior.EstadoEtiquetaId;
                        }
                    }
                }
                catch (Exception e)
                {
                    Logger.LogError($"Error al obtener información de etiqueta:\n{e}");
                }
            }
            return estadoEtiquetaId;
        }

        public bool ActualizarEtiquetaProcesada(EtiquetaAImprimir EtiquetaProcesada)
        {
            bool baseDeDatosActualizada = false;
            using(var dbEntities = new SGPv2_SGEntities())
            {
                try
                {
                    SG_MensajeRollo mensajeRollo = dbEntities.SG_MensajeRollo.SingleOrDefault(c => c.Id == EtiquetaProcesada.MensajeRolloId);
                    if (mensajeRollo != null)
                    {
                        mensajeRollo.EstadoEtiquetaId = EtiquetaProcesada.EstadoEtiquetaId;
                        dbEntities.SaveChanges();
                        baseDeDatosActualizada = true;
                    }
                }
                catch (Exception e)
                {
                    Logger.LogError($"Error al actualizar Base de Datos:\n{e}");
                }
            }
            return baseDeDatosActualizada;
        }

        public bool ActualizarEtiquetasProcesadas(List<EtiquetaAImprimir> ListaEtiquetasProcesadas)
        {
            List<int> listaMensajeRolloId = ListaEtiquetasProcesadas.Select(c => c.MensajeRolloId).ToList();
            bool baseDeDatosActualizada = false;
            using(var dbEntities = new SGPv2_SGEntities())
            {
                try
                {
                    IQueryable<SG_MensajeRollo> listaMensajesRollo = dbEntities.SG_MensajeRollo.Where(c => listaMensajeRolloId.Contains(c.Id));
                    foreach (var etiquetasProcesadas in ListaEtiquetasProcesadas)
                    {
                        foreach (var mensajesRollo in listaMensajesRollo)
                        {
                            if (etiquetasProcesadas.MensajeRolloId == mensajesRollo.Id) mensajesRollo.EstadoEtiquetaId = etiquetasProcesadas.EstadoEtiquetaId;
                        }
                    }
                    dbEntities.SaveChanges();
                    baseDeDatosActualizada = true;
                }
                catch (Exception e)
                {
                    Logger.LogError($"Error al actualizar Base de Datos:\n{e}");
                }
            }
            return baseDeDatosActualizada;
        }

        public List<T> EjecutarStoredProcedure<T>(string NombreStoredProcedure)
        {
            List<T> listaValoresObtenidos = new List<T>();
            try
            {
                using(var dbEntities = new SGPv2_SGEntities())
                {
                    listaValoresObtenidos = dbEntities.Database.SqlQuery<T>($"EXEC {NombreStoredProcedure}").ToList();
                }
            }
            catch (Exception e)
            {
                Logger.LogError($"Error al ejecutar Stored Procedure \"{NombreStoredProcedure}\":\n{e}");
            }
            return listaValoresObtenidos;
        }

        private List<List<string>> ObtenerValoresDeTablaDinamica(string NombreStoredProcedure)
        {
            var listaValoresDeTablaDinamica = new List<List<string>>();
            using (var dbEntities = new SGPv2_SGEntities())
            {
                var connection = dbEntities.Database.Connection;

                if (connection.State != ConnectionState.Open) connection.Open();

                using (var command = connection.CreateCommand())
                {
                    command.CommandText = NombreStoredProcedure;
                    command.Connection = connection;
                    using (var dataReader = command.ExecuteReader())
                    {
                        var listaColumnas = new List<string>();
                        if (dataReader.HasRows == true)
                        {
                            for (var indiceColumna = 0; indiceColumna < dataReader.VisibleFieldCount; indiceColumna++)
                            {
                                listaColumnas.Add(dataReader.GetName(indiceColumna));
                            }

                            while (dataReader.Read() == true)
                            {
                                var valoresDeTablaDinamica = new List<string>();
                                foreach (var columna in listaColumnas)
                                {
                                    string dataToAdd = (dataReader[columna] != null) ? dataReader[columna].ToString() : "";
                                    valoresDeTablaDinamica.AddRange(new List<string>() { columna, dataToAdd });
                                }
                                listaValoresDeTablaDinamica.Add(valoresDeTablaDinamica);
                            }
                        }
                    }
                }
            }
            return listaValoresDeTablaDinamica;
        }
    }
}
