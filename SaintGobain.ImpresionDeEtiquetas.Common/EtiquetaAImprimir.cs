﻿using System;

namespace SaintGobain.ImpresionDeEtiquetas.Common
{
    public class EtiquetaAImprimir
    {
        public int MensajeRolloId { get; set; }
        public DateTime? Fecha { get; set; }
        public string CodigoOrden { get; set; }
        public double? Longitud { get; set; }
        public double? Peso { get; set; }
        public double? Espesor { get; set; }
        public int? RolloNumero { get; set; }
        public string LoteNumero { get; set; }
        public string ArticuloNombre { get; set; }
        public string ArticuloCodigo { get; set; }
        public string ArticuloDescripcion { get; set; }
        public string UnidadProductivaNombre { get; set; }
        public string ImpresoraNombre { get; set; }
        public int TipoImpresoraId { get; set; }
        public string ImpresoraHostname { get; set; }
        public int ImpresoraPuerto { get; set; }
        public string TemplateAUtilizar { get; set; }
        public string TemplateZPL { get; set; }
        public string TemplateMarkemImaje { get; set; }
        public int EstadoEtiquetaId { get; set; }
        public PropiedadesDeEtiqueta PropiedadesDeEtiqueta { get; set; }
    }
}
