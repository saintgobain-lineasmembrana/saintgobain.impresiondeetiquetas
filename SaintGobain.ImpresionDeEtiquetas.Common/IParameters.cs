﻿namespace SaintGobain.ImpresionDeEtiquetas.Common
{
    public interface IParameters
    {
        bool GetBool(string NombreParametro, bool ValorDefault);
        int GetInt(string NombreParametro, int ValorDefault);
        string GetString(string NombreParametro, string ValorDefault);
    }
}
