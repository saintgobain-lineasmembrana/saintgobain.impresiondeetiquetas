﻿using System.Diagnostics;

[assembly: log4net.Config.XmlConfigurator(Watch = true)]

namespace SaintGobain.ImpresionDeEtiquetas.Common
{
    public static class Logger
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public static void LogTitle(string Mensaje)
        {
            log.Info(Mensaje);
        }

        public static void LogInfo(string Mensaje)
        {
            string mensajeDeLog = MensajeDeLog(Mensaje);
            log.Info(mensajeDeLog);
        }

        public static void LogDebug(string Mensaje)
        {
            string mensajeDeLog = MensajeDeLog(Mensaje);
            log.Debug(mensajeDeLog);
        }        
        
        public static void LogWarn(string Mensaje)
        {
            string mensajeDeLog = MensajeDeLog(Mensaje);
            log.Warn(mensajeDeLog);
        }

        public static void LogError(string Mensaje)
        {
            string mensajeDeLog = MensajeDeLog(Mensaje);
            log.Error(mensajeDeLog);
        }

        private static (string NombreClaseActual, string NombreMetodoActual) ObtenerNombresDeClaseYMetodoActual()
        {
            var stackTrace = new StackTrace();

            //Se busca el frame del Stack Trace para obtener el nombre de clase y método externo que llaman al Logger.
            int indiceLlamadoExterno = 2;
            string nombreClaseActual = stackTrace.GetFrame(indiceLlamadoExterno).GetMethod().DeclaringType.Name;
            string nombreMetodoActual = stackTrace.GetFrame(indiceLlamadoExterno).GetMethod().Name;

            return (nombreClaseActual, nombreMetodoActual);
        }

        private static string MensajeDeLog(string Mensaje)
        {
            (string nombreClaseActual, string nombreMetodoActual) = ObtenerNombresDeClaseYMetodoActual();
            string mensajeLog = $"{nombreClaseActual}.{nombreMetodoActual}() - {Mensaje}";
            return mensajeLog;
        }
    }
}
