﻿namespace SaintGobain.ImpresionDeEtiquetas.Common
{
    public class PropiedadesDeEtiqueta
    {
        public string ArticuloNombre { get; set; }
        public string CodigoQRBase64 { get; set; }
        public string TextoCuerpoLinea01 { get; set; }
        public string TextoCuerpoLinea02 { get; set; }
        public string TextoCuerpoLinea03 { get; set; }
        public string TextoCuerpoLinea04 { get; set; }
        public string TextoPieDeEtiqueta { get; set; }

        // Agregado por Ignacio Lacioppa 11/08/2022
        public string TamañoTextoCuerpoLinea01 { get; set; }
        public string TamañoTextoCuerpoLinea02 { get; set; }
        public string TamañoTextoCuerpoLinea03 { get; set; }
        public string TamañoTextoCuerpoLinea04 { get; set; }
        public string TamañoTextoPieDeEtiqueta { get; set; }

    }
}
