//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SaintGobain.ImpresionDeEtiquetas.Common.SGPv2_SGModel
{
    using System;
    using System.Collections.Generic;
    
    public partial class LPMImpresoraEquipo
    {
        public int Id { get; set; }
        public int ImpresoraId { get; set; }
        public int UnidadProductivaId { get; set; }
    
        public virtual LPMImpresora LPMImpresora { get; set; }
        public virtual UnidadProductiva UnidadProductiva { get; set; }
    }
}
