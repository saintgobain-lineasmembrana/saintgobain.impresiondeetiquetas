﻿using System;

namespace SaintGobain.ImpresionDeEtiquetas.Common
{
    public static class SafeGet
    {
        public static int GetInt(int? Value)
        {
            return (Value == null) ? 0 : (int)Value;
        }

        public static double GetDouble(double? Value)
        {
            return (Value == null) ? 0 : (double)Value;
        }

        public static string GetString(string Value)
        {
            return (Value == "" || Value == null) ? " " : Value;
        }

        public static DateTime GetDateTime(DateTime? Value)
        {
            return (Value == null) ? DateTime.MinValue : (DateTime)Value;
        }
    }
}
