﻿using System;
using System.IO;
using System.Net.Sockets;

namespace SaintGobain.ImpresionDeEtiquetas.Common
{
    public class TCPManager
    {
        private readonly TcpClient tcpClient;
        private readonly StreamWriter writer;
        private readonly StreamReader reader;
        private readonly string logIdentifacionConexion;
        private readonly int milisegundosTimeoutDeConexionTCP;

        public TCPManager(string Hostname, int PuertoTCP, int MilisegundosTimeoutDeConexionTCP = 5000)
        {
            try
            {
                logIdentifacionConexion = $"(Hostname: {Hostname}, PuertoTCP: {PuertoTCP})";
                milisegundosTimeoutDeConexionTCP = MilisegundosTimeoutDeConexionTCP;

                tcpClient = new TcpClient
                {
                    SendTimeout = milisegundosTimeoutDeConexionTCP,
                    ReceiveTimeout = milisegundosTimeoutDeConexionTCP
                };
                tcpClient.Connect(Hostname, PuertoTCP);

                writer = new StreamWriter(tcpClient.GetStream());
                reader = new StreamReader(tcpClient.GetStream());

                writer.BaseStream.WriteTimeout = milisegundosTimeoutDeConexionTCP;
                reader.BaseStream.ReadTimeout = milisegundosTimeoutDeConexionTCP;
            }
            catch (Exception e)
            {
                Logger.LogError($"Error al generar cliente TCP {logIdentifacionConexion}:\n{e}");
            }
        }

        public bool EnviarData(string DataAEnviar)
        {
            bool dataEnviada = false;
            if(tcpClient.Connected == true && writer != null)
            {
                try
                {
                    writer.Write(DataAEnviar);
                    writer.Flush();
                    dataEnviada = true;
                }
                catch(Exception e)
                {
                    Logger.LogError($"Error al escribir información al equipo/servidor {logIdentifacionConexion}:\n{e}");
                }
            }
            else
            {
                Logger.LogDebug($"Cliente TCP no conectado o \"writer = null\" {logIdentifacionConexion}.");
            }
            return dataEnviada;
        }

        public string RecibirRespuesta()
        {
            string respuestaRecibida = "";
            if(tcpClient.Connected == true && reader != null)
            {
                try
                {
                    var charBuffer = new char[256];
                    reader.Read(charBuffer, 0, charBuffer.Length);
                    if (charBuffer.Length > 0) respuestaRecibida = new string(charBuffer).TrimEnd('\0');
                }
                catch (Exception e)
                {
                    Logger.LogError($"Error al recibir respuesta del equipo/servidor {logIdentifacionConexion}:\n{e}");
                }
            }
            else
            {
                Logger.LogDebug($"Cliente TCP no conectado o \"reader = null\" {logIdentifacionConexion}.");
            }
            return respuestaRecibida;
        }

        public string EnviarDataYRecibirRespuesta(string DataAEnviar)
        {
            string respuestaRecibida = "";
            bool dataEnviada = EnviarData(DataAEnviar);
            if (dataEnviada == true) respuestaRecibida = RecibirRespuesta();
            return respuestaRecibida;
        }

        public void CerrarConexion()
        {
            if (tcpClient != null) tcpClient.Close();
            if (writer != null) writer.Close();
            if (reader != null) reader.Close();
        }
    }
}
