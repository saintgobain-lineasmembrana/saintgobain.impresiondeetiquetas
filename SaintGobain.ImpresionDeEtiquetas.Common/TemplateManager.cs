﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SaintGobain.ImpresionDeEtiquetas.Common
{
    public static class TemplateManager
    {
        public static List<string> ObtenerParametrosDeTemplateCSV(string TextoCSV, char CaracterDivisor = ',')
        {
            var listaValores = new List<string>();
            if (TextoCSV.Contains(CaracterDivisor) == true)
            {
                listaValores = TextoCSV.Split(CaracterDivisor).ToList();
            }
            else
            {
                Logger.LogWarn($"El parámetro string recibido no contiene '{CaracterDivisor}'. Parámetro string:\n{TextoCSV}");
            }
            return listaValores;
        }

        public static string GenerarParametrosEnTemplateCSV(string TemplateCSV, char CaracterDivisor = ',')
        {
            List<string> listaParametros = ObtenerParametrosDeTemplateCSV(TemplateCSV, CaracterDivisor);
            string templateConParametrosFormateados = "";
            if(listaParametros.Count > 0)
            {
                for (int i = 0;  i < listaParametros.Count; i++)
                {
                    if(i == 0)
                    {
                        templateConParametrosFormateados += listaParametros[i];
                    }
                    else
                    {
                        templateConParametrosFormateados += $"{listaParametros[i]}|{{{listaParametros[i]}}}";
                    }
                    if (i < listaParametros.Count - 1) templateConParametrosFormateados += ',';
                }
            }
            Logger.LogDebug(templateConParametrosFormateados);
            return templateConParametrosFormateados;
        }

        public static string AsignarValoresATemplateZPL(EtiquetaAImprimir EtiquetaAImprimir)
        {
            string templateConValores = ReemplazarParametrosPorValores(EtiquetaAImprimir);
            Logger.LogDebug($"Template generado:\n{templateConValores}");
            return templateConValores;
        }

        public static string AsignarValoresATemplateMarkemImaje(EtiquetaAImprimir EtiquetaAImprimir)
        {
            (string NombreParametroEnTemplate, string NombreAReemplazar) markemImajeRolloNumero = (ParameterManager.AppConfigParameters.GetString("MarkemImajeRolloNumero", "{TEXT_13}"), "{RolloNum}");
            (string NombreParametroEnTemplate, string NombreAReemplazar) markemImajeLoteNumero = (ParameterManager.AppConfigParameters.GetString("MarkemImajeLoteNumero", "{TEXT_Lote}"), "{LoteNum}");
            (string NombreParametroEnTemplate, string NombreAReemplazar) markemImajeOrdenProduccion = (ParameterManager.AppConfigParameters.GetString("MarkemImajeOrdenProduccion", "{TEXT_OP}"), "{CodOrden}");

            var listaNombreParametros = new List<(string NombreParametroEnTemplate, string NombreAReemplazar)>() { markemImajeRolloNumero, markemImajeOrdenProduccion, markemImajeLoteNumero };
            string templateConParametrosActualizados = GenerarParametrosEnTemplateCSV(EtiquetaAImprimir.TemplateAUtilizar);

            listaNombreParametros.ForEach(c => templateConParametrosActualizados = templateConParametrosActualizados.Replace(c.NombreParametroEnTemplate, c.NombreAReemplazar));
            string templateConValores = ReemplazarParametrosPorValores(EtiquetaAImprimir, templateConParametrosActualizados);

            Logger.LogDebug($"Template generado:\n{templateConValores}");

            return templateConValores;
        }

        public static string ObtenerTemplateAUtilizar(EtiquetaAImprimir EtiquetaAImprimir)
        {
            string templateAUtilizar;
            if (EtiquetaAImprimir.TipoImpresoraId == (int)TipoDeImpresora.MarkemImaje)
            {
                // Se resta -1 a "RolloNumero" porque el template integrado en la impresora suma 1 a la cuenta por cada impresión que hace.
                //EtiquetaAImprimir.RolloNumero -= 1;
                templateAUtilizar = EtiquetaAImprimir.TemplateMarkemImaje.Replace("{ArticuloNombre}", EtiquetaAImprimir.ArticuloNombre);              
            }
            else
            {
                templateAUtilizar = EtiquetaAImprimir.TemplateZPL;
            }
            return templateAUtilizar;
        }

        private static string ReemplazarParametrosPorValores(EtiquetaAImprimir EtiquetaAImprimir, string Template = "")
        {
            DateTime fecha = SafeGet.GetDateTime(EtiquetaAImprimir.Fecha);
            string codigoOrden = SafeGet.GetString(EtiquetaAImprimir.CodigoOrden);
            double espesor = SafeGet.GetDouble(EtiquetaAImprimir.Espesor);
            double longitud = SafeGet.GetDouble(EtiquetaAImprimir.Longitud);
            double peso = SafeGet.GetDouble(EtiquetaAImprimir.Peso);
            int rolloNumero = SafeGet.GetInt(EtiquetaAImprimir.RolloNumero);
            string loteNumero = SafeGet.GetString(EtiquetaAImprimir.LoteNumero);
            string articulooNombre = SafeGet.GetString(EtiquetaAImprimir.ArticuloNombre);
            string articulooCodigo = SafeGet.GetString(EtiquetaAImprimir.ArticuloCodigo);
            string articuloDescripcion = SafeGet.GetString(EtiquetaAImprimir.ArticuloDescripcion);
            string codigoQRBase64 = SafeGet.GetString(EtiquetaAImprimir?.PropiedadesDeEtiqueta?.CodigoQRBase64);
            string textoCuerpoLinea01 = SafeGet.GetString(EtiquetaAImprimir?.PropiedadesDeEtiqueta?.TextoCuerpoLinea01);
            string textoCuerpoLinea02 = SafeGet.GetString(EtiquetaAImprimir?.PropiedadesDeEtiqueta?.TextoCuerpoLinea02);
            string textoCuerpoLinea03 = SafeGet.GetString(EtiquetaAImprimir?.PropiedadesDeEtiqueta?.TextoCuerpoLinea03);
            string textoCuerpoLinea04 = SafeGet.GetString(EtiquetaAImprimir?.PropiedadesDeEtiqueta?.TextoCuerpoLinea04);
            string textoPieDeEtiqueta = SafeGet.GetString(EtiquetaAImprimir?.PropiedadesDeEtiqueta?.TextoPieDeEtiqueta);

            // Agregado por Ignacio Lacioppa 11/08/2022
            string tamanoTextoCuerpoLinea01 = SafeGet.GetString(EtiquetaAImprimir?.PropiedadesDeEtiqueta?.TamañoTextoCuerpoLinea01);
            string tamanoTextoCuerpoLinea02 = SafeGet.GetString(EtiquetaAImprimir?.PropiedadesDeEtiqueta?.TamañoTextoCuerpoLinea02);
            string tamanoTextoCuerpoLinea03 = SafeGet.GetString(EtiquetaAImprimir?.PropiedadesDeEtiqueta?.TamañoTextoCuerpoLinea03);
            string tamanoTextoCuerpoLinea04 = SafeGet.GetString(EtiquetaAImprimir?.PropiedadesDeEtiqueta?.TamañoTextoCuerpoLinea04);
            string tamanoTextoPieDeEtiqueta = SafeGet.GetString(EtiquetaAImprimir?.PropiedadesDeEtiqueta?.TamañoTextoPieDeEtiqueta);

            const string formatoFecha = "dd/MM/yyyy";
            const string formatoHora = "HH:mm:ss";
            string diaMesAnho = fecha.ToString(formatoFecha);
            string hora = fecha.ToString(formatoHora);

            string templateAActualizar;
            if (Template == "" || Template == null)
            {
                templateAActualizar = EtiquetaAImprimir.TemplateAUtilizar;
            }
            else
            {
                templateAActualizar = Template;
            }

            string templateConValores = templateAActualizar
                .Replace("{CodOrden}", codigoOrden)
                .Replace("{Es}", espesor.ToString())
                .Replace("{LoteNum}", loteNumero)
                .Replace("{Longitud}", longitud.ToString())
                .Replace("{Peso}", peso.ToString())
                .Replace("{ArticuloNombre}", articulooNombre)
                .Replace("{ArticuloCod}", articulooCodigo)
                .Replace("{ArticuloDesc}", articuloDescripcion)
                .Replace("{Fecha}", diaMesAnho.ToString())
                .Replace("{Hora}", hora.ToString())
                .Replace("{RolloNum}", rolloNumero.ToString("D6"))
                .Replace("{QRBase64}", codigoQRBase64)
                .Replace("{Txt01}", textoCuerpoLinea01)
                .Replace("{Txt02}", textoCuerpoLinea02)
                .Replace("{Txt03}", textoCuerpoLinea03)
                .Replace("{Txt04}", textoCuerpoLinea04)
                .Replace("{TxtPie}", textoPieDeEtiqueta)

                // Agregado por Ignacio Lacioppa 11/08/2022
                .Replace("{TxtTam01}", tamanoTextoCuerpoLinea01)
                .Replace("{TxtTam02}", tamanoTextoCuerpoLinea02)
                .Replace("{TxtTam03}", tamanoTextoCuerpoLinea03)
                .Replace("{TxtTam04}", tamanoTextoCuerpoLinea04)
                .Replace("{TxtTamPie}", tamanoTextoPieDeEtiqueta);

            return templateConValores;
        }
    }
}
