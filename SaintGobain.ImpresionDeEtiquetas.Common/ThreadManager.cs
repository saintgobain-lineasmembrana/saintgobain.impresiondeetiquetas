﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace SaintGobain.ImpresionDeEtiquetas.Common
{
    public static class ThreadManager
    {
        public static List<Thread> IniciarHilos()
        {
            var listaHilos = new List<Thread>();
            return listaHilos;
        }

        public static void DetenerHilos(List<Thread> ListaHilos)
        {
            string nombresDeHilos = ObtenerNombresDeHilos(ListaHilos);
            if (nombresDeHilos != string.Empty) Logger.LogDebug($"Hilos a detener: {nombresDeHilos}");
            foreach(var hilo in ListaHilos)
            {
                if (hilo.ThreadState != ThreadState.Aborted || hilo.ThreadState != ThreadState.AbortRequested) hilo.Abort();
            }
        }

        public static List<Thread> ObtenerHilosFinalizados(List<Thread> ListaHilos)
        {
            var listaHilosFinalizados = new List<Thread>();
            foreach (var hilo in ListaHilos)
            {
                if (hilo.IsAlive == false)
                {
                    listaHilosFinalizados.Add(hilo);
                    Logger.LogDebug($"Hilo finalizado: {hilo.ManagedThreadId}");
                }
            }
            return listaHilosFinalizados;
        }

        public static void EsperarPorHilosFinalizados(List<Thread> ListaHilos)
        {
            bool algunHiloActivo = true;
            while (algunHiloActivo == true)
            {
                Thread.Sleep(1000);
                algunHiloActivo = false;
                foreach(var hilo in ListaHilos)
                {
                    if (hilo.IsAlive == true) algunHiloActivo = true;
                }
            }
        }

        private static string ObtenerNombresDeHilos(List<Thread> ListaHilos)
        {
            string nombresDeHilos = string.Empty;
            if (ListaHilos.Count > 0)
            {
                List<string> listaNombresDeHilos = ListaHilos.Select(c => c.ManagedThreadId.ToString()).ToList();
                nombresDeHilos = string.Join(",", listaNombresDeHilos);
            }
            return nombresDeHilos;
        }
    }
}
