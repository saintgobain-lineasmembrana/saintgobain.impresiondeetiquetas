﻿namespace SaintGobain.ImpresionDeEtiquetas.FormularioDePruebas
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.Zebra = new System.Windows.Forms.TabPage();
            this.BtnPruebas = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.ZebraTextBoxRegistroDeMensajes = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.ZebraNumUpDownPuerto = new System.Windows.Forms.NumericUpDown();
            this.ZebraTextBoxHostname = new System.Windows.Forms.TextBox();
            this.ZebraTextBoxZPL = new System.Windows.Forms.TextBox();
            this.ZebraBtnImprimirEtiqueta = new System.Windows.Forms.Button();
            this.MarkemImaje = new System.Windows.Forms.TabPage();
            this.MINumUpDownNumeroRollo = new System.Windows.Forms.NumericUpDown();
            this.MITextBoxOrdenProduccion = new System.Windows.Forms.TextBox();
            this.MITextBoxNumeroLote = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.MITextBoxTemplateCSV = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.MITextBoxRespuestaEsperadaActualizarJob = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.MITextBoxRespuestaEsperadaSeleccionarJob = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.MITextBoxRespuestaEsperadaEjecutarImpresion = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.MITextBoxRespuestaEsperadaEstadoImpresoraListaParaImprimir = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.MITextBoxComandoEjecutarImpresion = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.MITextBoxComandoObtenerEstadoImpresoraListaParaImprimir = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.MITextBoxRegistroDeMensajes = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.MINumUpDownPuerto = new System.Windows.Forms.NumericUpDown();
            this.MITextBoxHostname = new System.Windows.Forms.TextBox();
            this.MIBtnImprimirEtiqueta = new System.Windows.Forms.Button();
            this.MITooltipInfoAImprimir = new System.Windows.Forms.ToolTip(this.components);
            this.tabControl1.SuspendLayout();
            this.Zebra.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ZebraNumUpDownPuerto)).BeginInit();
            this.MarkemImaje.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MINumUpDownNumeroRollo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MINumUpDownPuerto)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.Zebra);
            this.tabControl1.Controls.Add(this.MarkemImaje);
            this.tabControl1.Location = new System.Drawing.Point(12, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1067, 518);
            this.tabControl1.TabIndex = 4;
            // 
            // Zebra
            // 
            this.Zebra.Controls.Add(this.BtnPruebas);
            this.Zebra.Controls.Add(this.label5);
            this.Zebra.Controls.Add(this.ZebraTextBoxRegistroDeMensajes);
            this.Zebra.Controls.Add(this.label4);
            this.Zebra.Controls.Add(this.label3);
            this.Zebra.Controls.Add(this.label2);
            this.Zebra.Controls.Add(this.label1);
            this.Zebra.Controls.Add(this.ZebraNumUpDownPuerto);
            this.Zebra.Controls.Add(this.ZebraTextBoxHostname);
            this.Zebra.Controls.Add(this.ZebraTextBoxZPL);
            this.Zebra.Controls.Add(this.ZebraBtnImprimirEtiqueta);
            this.Zebra.Location = new System.Drawing.Point(4, 22);
            this.Zebra.Name = "Zebra";
            this.Zebra.Padding = new System.Windows.Forms.Padding(3);
            this.Zebra.Size = new System.Drawing.Size(1059, 492);
            this.Zebra.TabIndex = 0;
            this.Zebra.Text = "Zebra";
            this.Zebra.UseVisualStyleBackColor = true;
            // 
            // BtnPruebas
            // 
            this.BtnPruebas.Location = new System.Drawing.Point(253, 437);
            this.BtnPruebas.Name = "BtnPruebas";
            this.BtnPruebas.Size = new System.Drawing.Size(154, 37);
            this.BtnPruebas.TabIndex = 14;
            this.BtnPruebas.Text = "Botón de pruebas";
            this.BtnPruebas.UseVisualStyleBackColor = true;
            this.BtnPruebas.Visible = false;
            this.BtnPruebas.Click += new System.EventHandler(this.BtnPruebas_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(491, 12);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(111, 13);
            this.label5.TabIndex = 13;
            this.label5.Text = "Registro de mensajes:";
            // 
            // ZebraTextBoxRegistroDeMensajes
            // 
            this.ZebraTextBoxRegistroDeMensajes.Location = new System.Drawing.Point(494, 28);
            this.ZebraTextBoxRegistroDeMensajes.Multiline = true;
            this.ZebraTextBoxRegistroDeMensajes.Name = "ZebraTextBoxRegistroDeMensajes";
            this.ZebraTextBoxRegistroDeMensajes.ReadOnly = true;
            this.ZebraTextBoxRegistroDeMensajes.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.ZebraTextBoxRegistroDeMensajes.Size = new System.Drawing.Size(533, 427);
            this.ZebraTextBoxRegistroDeMensajes.TabIndex = 12;
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(466, 32);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(1, 444);
            this.label4.TabIndex = 11;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(40, 67);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(30, 13);
            this.label3.TabIndex = 10;
            this.label3.Text = "ZPL:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(250, 35);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "Puerto:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(40, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "Hostname:";
            // 
            // ZebraNumUpDownPuerto
            // 
            this.ZebraNumUpDownPuerto.Location = new System.Drawing.Point(297, 32);
            this.ZebraNumUpDownPuerto.Maximum = new decimal(new int[] {
            65535,
            0,
            0,
            0});
            this.ZebraNumUpDownPuerto.Name = "ZebraNumUpDownPuerto";
            this.ZebraNumUpDownPuerto.Size = new System.Drawing.Size(120, 20);
            this.ZebraNumUpDownPuerto.TabIndex = 7;
            // 
            // ZebraTextBoxHostname
            // 
            this.ZebraTextBoxHostname.Location = new System.Drawing.Point(104, 32);
            this.ZebraTextBoxHostname.Multiline = true;
            this.ZebraTextBoxHostname.Name = "ZebraTextBoxHostname";
            this.ZebraTextBoxHostname.Size = new System.Drawing.Size(124, 21);
            this.ZebraTextBoxHostname.TabIndex = 6;
            this.ZebraTextBoxHostname.TextChanged += new System.EventHandler(this.ZebraTextBoxHostname_TextChanged);
            // 
            // ZebraTextBoxZPL
            // 
            this.ZebraTextBoxZPL.Location = new System.Drawing.Point(38, 83);
            this.ZebraTextBoxZPL.Multiline = true;
            this.ZebraTextBoxZPL.Name = "ZebraTextBoxZPL";
            this.ZebraTextBoxZPL.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.ZebraTextBoxZPL.Size = new System.Drawing.Size(404, 329);
            this.ZebraTextBoxZPL.TabIndex = 5;
            // 
            // ZebraBtnImprimirEtiqueta
            // 
            this.ZebraBtnImprimirEtiqueta.Location = new System.Drawing.Point(38, 437);
            this.ZebraBtnImprimirEtiqueta.Name = "ZebraBtnImprimirEtiqueta";
            this.ZebraBtnImprimirEtiqueta.Size = new System.Drawing.Size(154, 37);
            this.ZebraBtnImprimirEtiqueta.TabIndex = 4;
            this.ZebraBtnImprimirEtiqueta.Text = "Imprimir etiqueta";
            this.ZebraBtnImprimirEtiqueta.UseVisualStyleBackColor = true;
            this.ZebraBtnImprimirEtiqueta.Click += new System.EventHandler(this.ZebraBtnImprimirEtiqueta_Click);
            // 
            // MarkemImaje
            // 
            this.MarkemImaje.Controls.Add(this.MINumUpDownNumeroRollo);
            this.MarkemImaje.Controls.Add(this.MITextBoxOrdenProduccion);
            this.MarkemImaje.Controls.Add(this.MITextBoxNumeroLote);
            this.MarkemImaje.Controls.Add(this.label22);
            this.MarkemImaje.Controls.Add(this.label21);
            this.MarkemImaje.Controls.Add(this.label20);
            this.MarkemImaje.Controls.Add(this.label19);
            this.MarkemImaje.Controls.Add(this.MITextBoxTemplateCSV);
            this.MarkemImaje.Controls.Add(this.label18);
            this.MarkemImaje.Controls.Add(this.label17);
            this.MarkemImaje.Controls.Add(this.MITextBoxRespuestaEsperadaActualizarJob);
            this.MarkemImaje.Controls.Add(this.label16);
            this.MarkemImaje.Controls.Add(this.MITextBoxRespuestaEsperadaSeleccionarJob);
            this.MarkemImaje.Controls.Add(this.label15);
            this.MarkemImaje.Controls.Add(this.MITextBoxRespuestaEsperadaEjecutarImpresion);
            this.MarkemImaje.Controls.Add(this.label14);
            this.MarkemImaje.Controls.Add(this.MITextBoxRespuestaEsperadaEstadoImpresoraListaParaImprimir);
            this.MarkemImaje.Controls.Add(this.label13);
            this.MarkemImaje.Controls.Add(this.MITextBoxComandoEjecutarImpresion);
            this.MarkemImaje.Controls.Add(this.label12);
            this.MarkemImaje.Controls.Add(this.MITextBoxComandoObtenerEstadoImpresoraListaParaImprimir);
            this.MarkemImaje.Controls.Add(this.label11);
            this.MarkemImaje.Controls.Add(this.label8);
            this.MarkemImaje.Controls.Add(this.label6);
            this.MarkemImaje.Controls.Add(this.MITextBoxRegistroDeMensajes);
            this.MarkemImaje.Controls.Add(this.label7);
            this.MarkemImaje.Controls.Add(this.label9);
            this.MarkemImaje.Controls.Add(this.label10);
            this.MarkemImaje.Controls.Add(this.MINumUpDownPuerto);
            this.MarkemImaje.Controls.Add(this.MITextBoxHostname);
            this.MarkemImaje.Controls.Add(this.MIBtnImprimirEtiqueta);
            this.MarkemImaje.Location = new System.Drawing.Point(4, 22);
            this.MarkemImaje.Name = "MarkemImaje";
            this.MarkemImaje.Padding = new System.Windows.Forms.Padding(3);
            this.MarkemImaje.Size = new System.Drawing.Size(1059, 492);
            this.MarkemImaje.TabIndex = 1;
            this.MarkemImaje.Text = "Markem Imaje";
            this.MarkemImaje.UseVisualStyleBackColor = true;
            // 
            // MINumUpDownNumeroRollo
            // 
            this.MINumUpDownNumeroRollo.Location = new System.Drawing.Point(86, 326);
            this.MINumUpDownNumeroRollo.Maximum = new decimal(new int[] {
            65535,
            0,
            0,
            0});
            this.MINumUpDownNumeroRollo.Name = "MINumUpDownNumeroRollo";
            this.MINumUpDownNumeroRollo.Size = new System.Drawing.Size(120, 20);
            this.MINumUpDownNumeroRollo.TabIndex = 47;
            // 
            // MITextBoxOrdenProduccion
            // 
            this.MITextBoxOrdenProduccion.Location = new System.Drawing.Point(152, 359);
            this.MITextBoxOrdenProduccion.Multiline = true;
            this.MITextBoxOrdenProduccion.Name = "MITextBoxOrdenProduccion";
            this.MITextBoxOrdenProduccion.Size = new System.Drawing.Size(124, 21);
            this.MITextBoxOrdenProduccion.TabIndex = 46;
            // 
            // MITextBoxNumeroLote
            // 
            this.MITextBoxNumeroLote.Location = new System.Drawing.Point(303, 325);
            this.MITextBoxNumeroLote.Multiline = true;
            this.MITextBoxNumeroLote.Name = "MITextBoxNumeroLote";
            this.MITextBoxNumeroLote.Size = new System.Drawing.Size(124, 21);
            this.MITextBoxNumeroLote.TabIndex = 44;
            // 
            // label22
            // 
            this.label22.BackColor = System.Drawing.Color.Black;
            this.label22.Location = new System.Drawing.Point(37, 390);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(379, 1);
            this.label22.TabIndex = 43;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(245, 328);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(46, 13);
            this.label21.TabIndex = 42;
            this.label21.Text = "N° Lote:";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(37, 361);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(111, 13);
            this.label20.TabIndex = 41;
            this.label20.Text = "Orden de Producción:";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(37, 328);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(49, 13);
            this.label19.TabIndex = 40;
            this.label19.Text = "N° Rollo:";
            // 
            // MITextBoxTemplateCSV
            // 
            this.MITextBoxTemplateCSV.Location = new System.Drawing.Point(153, 403);
            this.MITextBoxTemplateCSV.Multiline = true;
            this.MITextBoxTemplateCSV.Name = "MITextBoxTemplateCSV";
            this.MITextBoxTemplateCSV.Size = new System.Drawing.Size(286, 20);
            this.MITextBoxTemplateCSV.TabIndex = 39;
            this.MITooltipInfoAImprimir.SetToolTip(this.MITextBoxTemplateCSV, "Escribir template a imprimir mediante la siguiente sintaxis:\r\nNombreTemplate,Para" +
        "metro1,Parametro2,Parametro3,...,ParametroN\r\n");
            this.MITextBoxTemplateCSV.TextChanged += new System.EventHandler(this.MITextBoxTemplateCSV_TextChanged);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(35, 406);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(78, 13);
            this.label18.TabIndex = 38;
            this.label18.Text = "Template CSV:";
            // 
            // label17
            // 
            this.label17.BackColor = System.Drawing.Color.Black;
            this.label17.Location = new System.Drawing.Point(36, 312);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(379, 1);
            this.label17.TabIndex = 37;
            // 
            // MITextBoxRespuestaEsperadaActualizarJob
            // 
            this.MITextBoxRespuestaEsperadaActualizarJob.Location = new System.Drawing.Point(303, 275);
            this.MITextBoxRespuestaEsperadaActualizarJob.Multiline = true;
            this.MITextBoxRespuestaEsperadaActualizarJob.Name = "MITextBoxRespuestaEsperadaActualizarJob";
            this.MITextBoxRespuestaEsperadaActualizarJob.Size = new System.Drawing.Size(124, 21);
            this.MITextBoxRespuestaEsperadaActualizarJob.TabIndex = 36;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(37, 278);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(169, 13);
            this.label16.TabIndex = 35;
            this.label16.Text = "RespuestaEsperadaActualizarJob:";
            // 
            // MITextBoxRespuestaEsperadaSeleccionarJob
            // 
            this.MITextBoxRespuestaEsperadaSeleccionarJob.Location = new System.Drawing.Point(303, 239);
            this.MITextBoxRespuestaEsperadaSeleccionarJob.Multiline = true;
            this.MITextBoxRespuestaEsperadaSeleccionarJob.Name = "MITextBoxRespuestaEsperadaSeleccionarJob";
            this.MITextBoxRespuestaEsperadaSeleccionarJob.Size = new System.Drawing.Size(124, 21);
            this.MITextBoxRespuestaEsperadaSeleccionarJob.TabIndex = 34;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(37, 242);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(179, 13);
            this.label15.TabIndex = 33;
            this.label15.Text = "RespuestaEsperadaSeleccionarJob:";
            // 
            // MITextBoxRespuestaEsperadaEjecutarImpresion
            // 
            this.MITextBoxRespuestaEsperadaEjecutarImpresion.Location = new System.Drawing.Point(303, 203);
            this.MITextBoxRespuestaEsperadaEjecutarImpresion.Multiline = true;
            this.MITextBoxRespuestaEsperadaEjecutarImpresion.Name = "MITextBoxRespuestaEsperadaEjecutarImpresion";
            this.MITextBoxRespuestaEsperadaEjecutarImpresion.Size = new System.Drawing.Size(124, 21);
            this.MITextBoxRespuestaEsperadaEjecutarImpresion.TabIndex = 32;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(37, 206);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(190, 13);
            this.label14.TabIndex = 31;
            this.label14.Text = "RespuestaEsperadaEjecutarImpresion:";
            // 
            // MITextBoxRespuestaEsperadaEstadoImpresoraListaParaImprimir
            // 
            this.MITextBoxRespuestaEsperadaEstadoImpresoraListaParaImprimir.Location = new System.Drawing.Point(303, 166);
            this.MITextBoxRespuestaEsperadaEstadoImpresoraListaParaImprimir.Multiline = true;
            this.MITextBoxRespuestaEsperadaEstadoImpresoraListaParaImprimir.Name = "MITextBoxRespuestaEsperadaEstadoImpresoraListaParaImprimir";
            this.MITextBoxRespuestaEsperadaEstadoImpresoraListaParaImprimir.Size = new System.Drawing.Size(124, 21);
            this.MITextBoxRespuestaEsperadaEstadoImpresoraListaParaImprimir.TabIndex = 30;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(37, 169);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(264, 13);
            this.label13.TabIndex = 29;
            this.label13.Text = "RespuestaEsperadaEstadoImpresoraListaParaImprimir:";
            // 
            // MITextBoxComandoEjecutarImpresion
            // 
            this.MITextBoxComandoEjecutarImpresion.Location = new System.Drawing.Point(303, 129);
            this.MITextBoxComandoEjecutarImpresion.Multiline = true;
            this.MITextBoxComandoEjecutarImpresion.Name = "MITextBoxComandoEjecutarImpresion";
            this.MITextBoxComandoEjecutarImpresion.Size = new System.Drawing.Size(124, 21);
            this.MITextBoxComandoEjecutarImpresion.TabIndex = 28;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(37, 132);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(139, 13);
            this.label12.TabIndex = 27;
            this.label12.Text = "ComandoEjecutarImpresion:";
            // 
            // MITextBoxComandoObtenerEstadoImpresoraListaParaImprimir
            // 
            this.MITextBoxComandoObtenerEstadoImpresoraListaParaImprimir.Location = new System.Drawing.Point(303, 93);
            this.MITextBoxComandoObtenerEstadoImpresoraListaParaImprimir.Multiline = true;
            this.MITextBoxComandoObtenerEstadoImpresoraListaParaImprimir.Name = "MITextBoxComandoObtenerEstadoImpresoraListaParaImprimir";
            this.MITextBoxComandoObtenerEstadoImpresoraListaParaImprimir.Size = new System.Drawing.Size(124, 21);
            this.MITextBoxComandoObtenerEstadoImpresoraListaParaImprimir.TabIndex = 26;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(37, 96);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(251, 13);
            this.label11.TabIndex = 25;
            this.label11.Text = "ComandoObtenerEstadoImpresoraListaParaImprimir:";
            this.label11.Click += new System.EventHandler(this.label11_Click);
            // 
            // label8
            // 
            this.label8.BackColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(37, 78);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(379, 1);
            this.label8.TabIndex = 24;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(491, 12);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(111, 13);
            this.label6.TabIndex = 23;
            this.label6.Text = "Registro de mensajes:";
            // 
            // MITextBoxRegistroDeMensajes
            // 
            this.MITextBoxRegistroDeMensajes.Location = new System.Drawing.Point(494, 28);
            this.MITextBoxRegistroDeMensajes.Multiline = true;
            this.MITextBoxRegistroDeMensajes.Name = "MITextBoxRegistroDeMensajes";
            this.MITextBoxRegistroDeMensajes.ReadOnly = true;
            this.MITextBoxRegistroDeMensajes.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.MITextBoxRegistroDeMensajes.Size = new System.Drawing.Size(533, 427);
            this.MITextBoxRegistroDeMensajes.TabIndex = 22;
            // 
            // label7
            // 
            this.label7.BackColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(463, 34);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(1, 444);
            this.label7.TabIndex = 21;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(250, 35);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(41, 13);
            this.label9.TabIndex = 19;
            this.label9.Text = "Puerto:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(40, 34);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(58, 13);
            this.label10.TabIndex = 18;
            this.label10.Text = "Hostname:";
            // 
            // MINumUpDownPuerto
            // 
            this.MINumUpDownPuerto.Location = new System.Drawing.Point(297, 32);
            this.MINumUpDownPuerto.Maximum = new decimal(new int[] {
            65535,
            0,
            0,
            0});
            this.MINumUpDownPuerto.Name = "MINumUpDownPuerto";
            this.MINumUpDownPuerto.Size = new System.Drawing.Size(120, 20);
            this.MINumUpDownPuerto.TabIndex = 17;
            // 
            // MITextBoxHostname
            // 
            this.MITextBoxHostname.Location = new System.Drawing.Point(104, 32);
            this.MITextBoxHostname.Multiline = true;
            this.MITextBoxHostname.Name = "MITextBoxHostname";
            this.MITextBoxHostname.Size = new System.Drawing.Size(124, 21);
            this.MITextBoxHostname.TabIndex = 16;
            // 
            // MIBtnImprimirEtiqueta
            // 
            this.MIBtnImprimirEtiqueta.Location = new System.Drawing.Point(38, 437);
            this.MIBtnImprimirEtiqueta.Name = "MIBtnImprimirEtiqueta";
            this.MIBtnImprimirEtiqueta.Size = new System.Drawing.Size(154, 37);
            this.MIBtnImprimirEtiqueta.TabIndex = 14;
            this.MIBtnImprimirEtiqueta.Text = "Imprimir etiqueta";
            this.MIBtnImprimirEtiqueta.UseVisualStyleBackColor = true;
            this.MIBtnImprimirEtiqueta.Click += new System.EventHandler(this.MIBtnImprimirEtiqueta_Click);
            // 
            // MITooltipInfoAImprimir
            // 
            this.MITooltipInfoAImprimir.AutoPopDelay = 5000;
            this.MITooltipInfoAImprimir.InitialDelay = 1000;
            this.MITooltipInfoAImprimir.ReshowDelay = 100;
            this.MITooltipInfoAImprimir.Popup += new System.Windows.Forms.PopupEventHandler(this.MITooltipInfoAImprimir_Popup);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1091, 542);
            this.Controls.Add(this.tabControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.Text = "Saint Gobain - Impresion de Etiquetas: Formulario de pruebas";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.tabControl1.ResumeLayout(false);
            this.Zebra.ResumeLayout(false);
            this.Zebra.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ZebraNumUpDownPuerto)).EndInit();
            this.MarkemImaje.ResumeLayout(false);
            this.MarkemImaje.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MINumUpDownNumeroRollo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MINumUpDownPuerto)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage Zebra;
        private System.Windows.Forms.TabPage MarkemImaje;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown ZebraNumUpDownPuerto;
        private System.Windows.Forms.TextBox ZebraTextBoxHostname;
        private System.Windows.Forms.TextBox ZebraTextBoxZPL;
        private System.Windows.Forms.Button ZebraBtnImprimirEtiqueta;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox ZebraTextBoxRegistroDeMensajes;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox MITextBoxRegistroDeMensajes;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.NumericUpDown MINumUpDownPuerto;
        private System.Windows.Forms.TextBox MITextBoxHostname;
        private System.Windows.Forms.Button MIBtnImprimirEtiqueta;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox MITextBoxComandoObtenerEstadoImpresoraListaParaImprimir;
        private System.Windows.Forms.TextBox MITextBoxRespuestaEsperadaEjecutarImpresion;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox MITextBoxRespuestaEsperadaEstadoImpresoraListaParaImprimir;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox MITextBoxComandoEjecutarImpresion;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox MITextBoxRespuestaEsperadaActualizarJob;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox MITextBoxRespuestaEsperadaSeleccionarJob;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox MITextBoxTemplateCSV;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.ToolTip MITooltipInfoAImprimir;
        private System.Windows.Forms.TextBox MITextBoxOrdenProduccion;
        private System.Windows.Forms.TextBox MITextBoxNumeroLote;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.NumericUpDown MINumUpDownNumeroRollo;
        private System.Windows.Forms.Button BtnPruebas;
    }
}

