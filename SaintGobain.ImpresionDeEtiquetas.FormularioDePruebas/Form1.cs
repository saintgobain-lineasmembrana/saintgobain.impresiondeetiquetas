﻿using SaintGobain.ImpresionDeEtiquetas.Common;
using SaintGobain.ImpresionDeEtiquetas.Repository;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace SaintGobain.ImpresionDeEtiquetas.FormularioDePruebas
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            MITextBoxComandoObtenerEstadoImpresoraListaParaImprimir.Text = "~PS|1|";
            MITextBoxComandoEjecutarImpresion.Text = "~PG|0|1|";
            MITextBoxRespuestaEsperadaEstadoImpresoraListaParaImprimir.Text = "~PR0|0|";
            MITextBoxRespuestaEsperadaEjecutarImpresion.Text = "~PG0|";
            MITextBoxRespuestaEsperadaSeleccionarJob.Text = "~JS0|";
            MITextBoxRespuestaEsperadaActualizarJob.Text = "~JU0|";
            MITextBoxNumeroLote.Text = "LOTE-01";
            MINumUpDownNumeroRollo.Value = 1;
            MITextBoxOrdenProduccion.Text = "OP-01";
            MITextBoxTemplateCSV.Text = "NombreTemplate,TEXT_13,TEXT_Lote,TEXT_OP";
            MITextBoxHostname.Text = "127.0.0.1";
            MINumUpDownPuerto.Value = 21000;
            ZebraTextBoxHostname.Text = "127.0.0.1";
            ZebraNumUpDownPuerto.Value = 9100;
        }

        private void EscribirTextboxRegistroDeMensajes(TextBox TextBox, string MensajeAEscribir)
        {
            string newline = Environment.NewLine;
            string cabeceraRegistroDeMensaje = $"=={DateTime.Now:dd/MM/yyyy HH:mm:ss}==";
            TextBox.Text = $"{cabeceraRegistroDeMensaje}{newline}{MensajeAEscribir}{newline}{newline}" + TextBox.Text;
        }

        private void ZebraBtnImprimirEtiqueta_Click(object sender, EventArgs e)
        {
            string hostname = ZebraTextBoxHostname.Text;
            int puerto = (int)ZebraNumUpDownPuerto.Value;
            int milisegundosTimeoutDeConexionTCP = ParameterManager.AppConfigParameters.GetInt("MilisegundosTimeoutDeConexionTCP", 5000);
            string mensajeHostnameYPuerto = $"(Hostname: {hostname}, Puerto: {puerto})";
            if (hostname == "" || puerto == 0)
            {
                MessageBox.Show("Introduzca parámetros válidos de conexión (Hostname y Puerto).");
            }
            else
            {
                string zpl = ZebraTextBoxZPL.Text;
                var impresoraZebra = new ImpresoraZebra(hostname, puerto, milisegundosTimeoutDeConexionTCP);
                bool impresionExitosa = impresoraZebra.ImprimirEtiqueta(zpl);
                if (impresionExitosa == true) 
                {
                    EscribirTextboxRegistroDeMensajes(ZebraTextBoxRegistroDeMensajes, $"Etiqueta impresa exitosamente {mensajeHostnameYPuerto}.");
                }
                else
                {
                    EscribirTextboxRegistroDeMensajes(ZebraTextBoxRegistroDeMensajes, $"No se pudo imprimir la etiqueta. Ver log para más información {mensajeHostnameYPuerto}.");
                }
            }
        }

        private void ZebraTextBoxHostname_TextChanged(object sender, EventArgs e)
        {

        }

        private void label11_Click(object sender, EventArgs e)
        {

        }

        private void MITooltipInfoAImprimir_Popup(object sender, PopupEventArgs e)
        {

        }

        private void MIBtnImprimirEtiqueta_Click(object sender, EventArgs e)
        {
            string hostname = MITextBoxHostname.Text;
            int puerto = (int)MINumUpDownPuerto.Value;
            int milisegundosTimeoutDeConexionTCP = ParameterManager.AppConfigParameters.GetInt("MilisegundosTimeoutDeConexionTCP", 5000);
            string mensajeHostnameYPuerto = $"(Hostname: {hostname}, Puerto: {puerto})";
            string comandoObtenerEstadoImpresoraListaParaImprimir = MITextBoxComandoObtenerEstadoImpresoraListaParaImprimir.Text;
            string comandoEjecutarImpresion = MITextBoxComandoEjecutarImpresion.Text;
            string respuestaEsperadaEstadoImpresoraListaParaImprimir = MITextBoxRespuestaEsperadaEstadoImpresoraListaParaImprimir.Text;
            string respuestaEsperadaEjecutarImpresion = MITextBoxRespuestaEsperadaEjecutarImpresion.Text;
            string respuestaEsperadaSeleccionarJob = MITextBoxRespuestaEsperadaSeleccionarJob.Text;
            string respuestaEsperadaActualizarJob = MITextBoxRespuestaEsperadaActualizarJob.Text;
            var etiquetaAImprimir = new EtiquetaAImprimir()
            {
                RolloNumero = (int?)MINumUpDownNumeroRollo.Value,
                CodigoOrden = MITextBoxOrdenProduccion.Text,
                LoteNumero = MITextBoxNumeroLote.Text,
                TemplateMarkemImaje = MITextBoxTemplateCSV.Text,
                TemplateAUtilizar = MITextBoxTemplateCSV.Text,
                TipoImpresoraId = 2
            };

            if (hostname == "" || puerto == 0)
            {
                MessageBox.Show("Introduzca parámetros válidos de conexión (Hostname y Puerto).");
            }
            else
            {
                var impresoraMarkemImage = new ImpresoraMarkemImaje(hostname, puerto, milisegundosTimeoutDeConexionTCP, comandoObtenerEstadoImpresoraListaParaImprimir, comandoEjecutarImpresion, respuestaEsperadaEstadoImpresoraListaParaImprimir, respuestaEsperadaEjecutarImpresion, respuestaEsperadaSeleccionarJob, respuestaEsperadaActualizarJob);
                bool impresionExitosa = impresoraMarkemImage.ImprimirEtiqueta(etiquetaAImprimir);
                if (impresionExitosa == true)
                {
                    EscribirTextboxRegistroDeMensajes(MITextBoxRegistroDeMensajes, $"Etiqueta impresa exitosamente {mensajeHostnameYPuerto}.");
                }
                else
                {
                    EscribirTextboxRegistroDeMensajes(MITextBoxRegistroDeMensajes, $"No se pudo imprimir la etiqueta. Ver log para más información {mensajeHostnameYPuerto}.");
                }
            }
        }

        private void BtnPruebas_Click(object sender, EventArgs e)
        {
            var db = new DatabaseManager();
            db.GetEstadoEtiquetaDeRolloAnterior("T2021-11-26", 137);
        }

        private void MITextBoxTemplateCSV_TextChanged(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}
