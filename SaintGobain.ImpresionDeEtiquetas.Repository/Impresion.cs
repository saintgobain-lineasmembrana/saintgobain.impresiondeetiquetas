﻿using SaintGobain.ImpresionDeEtiquetas.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace SaintGobain.ImpresionDeEtiquetas.Repository
{
    public class Impresion
    {
        private readonly int milisegundosTimeoutDeConexionTCP;
        private readonly DatabaseManager databaseManager;

        public Impresion(int MilisegundosTimeoutDeConexionTCP)
        {
            milisegundosTimeoutDeConexionTCP = MilisegundosTimeoutDeConexionTCP; 
            databaseManager = new DatabaseManager();
        }

        public void ImprimirEtiquetasNoProcesadasPorHilos()
        {
            var listaEtiquetasAImprimir = databaseManager.GetEtiquetasAImprimir();
            if (listaEtiquetasAImprimir.Count > 0)
            {
                Logger.LogTitle("--------------------- Inicio de Impresión de Etiquetas No Procesadas ---------------------");
            }
            else
            {
                return;
            }

            var listaEtiquetasUnicasAImprimir = ObtenerEtiquetasUnicas(listaEtiquetasAImprimir);
            Logger.LogInfo($"Cantidad de etiquetas a imprimir: {listaEtiquetasUnicasAImprimir.Count}.");
            if(listaEtiquetasUnicasAImprimir.Count > 0)
            {
                var listaUnidadesProductivas = listaEtiquetasUnicasAImprimir.GroupBy(c => c.UnidadProductivaNombre).Select(group => group.First().UnidadProductivaNombre);
                List<Thread> listaHilos = new List<Thread>();
                try
                {
                    listaHilos = IniciarHilosPorImpresora(listaEtiquetasUnicasAImprimir);
                    while (listaHilos.Count > 0)
                    {
                        Thread.Sleep(1000);
                        List<Thread> listaHilosFinalizados = ThreadManager.ObtenerHilosFinalizados(listaHilos);
                        if (listaHilosFinalizados.Count > 0)
                        {
                            foreach (var hiloFinalizado in listaHilosFinalizados) listaHilos.Remove(hiloFinalizado);
                        }
                    }
                }
                catch (Exception e)
                {
                    Logger.LogError($"Se ha recibido una orden para abortar los hilos de impresión:\n{e}");
                }
                finally
                {
                    if (listaHilos.Count > 0)
                    {
                        ThreadManager.DetenerHilos(listaHilos);
                        ThreadManager.EsperarPorHilosFinalizados(listaHilos);
                    }
                }
            }
            else
            {
                Logger.LogInfo("No se encontraron etiquetas para imprimir.");
            }

            Logger.LogTitle("--------------------- Fin de Impresión de Etiquetas No Procesadas ---------------------");
        }

        public void ImprimirEtiquetasNoProcesadas(List<EtiquetaAImprimir> ListaEtiquetasAImprimir)
        {
            if (ListaEtiquetasAImprimir.Count > 0)
            {
                Logger.LogInfo("Imprimiendo etiquetas.");
                var listaEtiquetasProcesadas = ImprimirEtiquetas(ListaEtiquetasAImprimir);
                bool etiquetasActualizadasEnBaseDeDatos = databaseManager.ActualizarEtiquetasProcesadas(listaEtiquetasProcesadas);
                if (etiquetasActualizadasEnBaseDeDatos == true) Logger.LogInfo("Etiquetas actualizadas en Base de Datos.");
            }
            else
            {
                Logger.LogInfo("No se encontraron etiquetas para imprimir.");
            }
        }

        public List<EtiquetaAImprimir> ImprimirEtiquetas(List<EtiquetaAImprimir> ListaEtiquetasAImprimir)
        {
            var listaEtiquetasProcesadas = new List<EtiquetaAImprimir>();
            foreach (var etiquetaAImprimir in ListaEtiquetasAImprimir)
            {
                var etiquetaProcesada = ImprimirEtiqueta(etiquetaAImprimir);
                listaEtiquetasProcesadas.Add(etiquetaProcesada);
                // Espera de 3seg entre impresión de etiquetas.
                Thread.Sleep(3000);
            }
            return listaEtiquetasProcesadas;
        }

        public EtiquetaAImprimir ImprimirEtiqueta(EtiquetaAImprimir EtiquetaAImprimir)
        {
            string logInfoEtiqueta = $"(MensajeRolloId: {EtiquetaAImprimir.MensajeRolloId}, ImpresoraNombre: {EtiquetaAImprimir.ImpresoraNombre}, UnidadProductivaNombre: {EtiquetaAImprimir.UnidadProductivaNombre}, ArticuloNombre: {EtiquetaAImprimir.ArticuloNombre}, RolloNumero: {EtiquetaAImprimir.RolloNumero})";
            Logger.LogInfo($"Imprimiendo etiqueta {logInfoEtiqueta}");
            bool impresionExitosa = ImpresionDeEtiquetaSegunImpresora(EtiquetaAImprimir);
            EtiquetaAImprimir etiquetaProcesada = EtiquetaAImprimir;
            if (impresionExitosa == true)
            {
                etiquetaProcesada.EstadoEtiquetaId = (int)EstadoEtiqueta.Impresa;
                Logger.LogInfo($"Etiqueta impresa {logInfoEtiqueta}.");
            }
            else
            {
                etiquetaProcesada.EstadoEtiquetaId = (int)EstadoEtiqueta.NoImpresa;
                Logger.LogWarn($"No se pudo imprimir etiqueta {logInfoEtiqueta}.");
            }
            return etiquetaProcesada;
        }

        private bool ImpresionDeEtiquetaSegunImpresora(EtiquetaAImprimir EtiquetaAImprimir)
        {
            bool impresionExitosa = false;
            var tipoDeImpresora = (TipoDeImpresora)EtiquetaAImprimir.TipoImpresoraId;
            var fechaActual = DateTime.Now;
            var fechaEtiqueta = SafeGet.GetDateTime(EtiquetaAImprimir.Fecha);
            double minutosDiferenciaMaximosParaImpresionDeEtiqueta = ParameterManager.AppConfigParameters.GetDouble("MinutosDiferenciaMaximosParaImpresionDeEtiqueta", 15);
            double diferenciaFechasEnMinutos = (fechaActual - fechaEtiqueta).TotalMinutes;
            if (diferenciaFechasEnMinutos > minutosDiferenciaMaximosParaImpresionDeEtiqueta) 
            { 
                Logger.LogWarn($"Etiqueta con fecha antigua. No se imprime (Fecha de etiqueta = {fechaEtiqueta}, MinutosDiferenciaMaximosParaImpresionDeEtiqueta = {minutosDiferenciaMaximosParaImpresionDeEtiqueta}).");
                return impresionExitosa;
            }
            switch (tipoDeImpresora)
            {
                case TipoDeImpresora.Zebra:
                    var impresoraZebra = new ImpresoraZebra(EtiquetaAImprimir.ImpresoraHostname, EtiquetaAImprimir.ImpresoraPuerto, milisegundosTimeoutDeConexionTCP);
                    impresionExitosa = impresoraZebra.ImprimirEtiqueta(EtiquetaAImprimir);
                    break;
                case TipoDeImpresora.MarkemImaje:
                    var databaseManager = new DatabaseManager();
                    var estadoEtiquetaAnterior = databaseManager.GetEstadoEtiquetaDeRolloAnterior(EtiquetaAImprimir.LoteNumero, EtiquetaAImprimir.RolloNumero);
                    if (estadoEtiquetaAnterior != (int)EstadoEtiqueta.Impresa)
                    {
                        var impresoraMarkemImaje = new ImpresoraMarkemImaje(EtiquetaAImprimir.ImpresoraHostname, EtiquetaAImprimir.ImpresoraPuerto, milisegundosTimeoutDeConexionTCP);
                        impresionExitosa = impresoraMarkemImaje.ImprimirEtiqueta(EtiquetaAImprimir);
                    }
                    else
                    {
                        impresionExitosa = true;
                    }

                    EtiquetaAImprimir etiquetaMarkemImajeProcesada = EtiquetaAImprimir;
                    if (impresionExitosa == true)
                    {
                        etiquetaMarkemImajeProcesada.EstadoEtiquetaId = (int)EstadoEtiqueta.Impresa;
                    }
                    else
                    {
                        etiquetaMarkemImajeProcesada.EstadoEtiquetaId = (int)EstadoEtiqueta.NoImpresa;
                    }
                    databaseManager.ActualizarEtiquetaProcesada(etiquetaMarkemImajeProcesada);
                    break;
                default:
                    break;
            }
            return impresionExitosa;
        }

        private List<EtiquetaAImprimir> ObtenerEtiquetasUnicas(List<EtiquetaAImprimir> ListaEtiquetasAImprimir)
        {
            var listaEtiquetasAImprimirUnicas = new List<EtiquetaAImprimir>();
            if (ListaEtiquetasAImprimir != null)
            {
                List<int> listaMensajeRolloID = ListaEtiquetasAImprimir.Select(c => c.MensajeRolloId).ToList();
                foreach (var mensajeRolloID in listaMensajeRolloID)
                {
                    var etiquetaAImprimirSegunId = ListaEtiquetasAImprimir.Where(c => c.MensajeRolloId == mensajeRolloID).ToList();
                    if (etiquetaAImprimirSegunId.Count() == 1)
                    {
                        listaEtiquetasAImprimirUnicas.AddRange(etiquetaAImprimirSegunId);
                    }
                    else
                    {
                        Logger.LogWarn($"Se obtuvo más de una entrada para un mismo Id de rollo (MensajeRolloId: {mensajeRolloID}).");
                    }
                }
            }
            return listaEtiquetasAImprimirUnicas;
        }

        private List<Thread> IniciarHilosPorImpresora(List<EtiquetaAImprimir> listaEtiquetasUnicasAImprimir)
        {
            var listaUnidadesProductivas = listaEtiquetasUnicasAImprimir.GroupBy(c => c.UnidadProductivaNombre).Select(group => group.First().UnidadProductivaNombre);
            List<Thread> listaHilos = new List<Thread>();
            foreach (var unidadProductiva in listaUnidadesProductivas)
            {
                var listaEtiquetasPorUnidadProductiva = listaEtiquetasUnicasAImprimir.Where(c => c.UnidadProductivaNombre == unidadProductiva).OrderBy(c => c.RolloNumero).ToList();
                var hilo = new Thread(new ThreadStart(() => ImprimirEtiquetasNoProcesadas(listaEtiquetasPorUnidadProductiva)));
                Logger.LogDebug($"Iniciando hilo: {hilo.ManagedThreadId}");
                hilo.Start();
                listaHilos.Add(hilo);
            }
            return listaHilos;
        }
    }
}
