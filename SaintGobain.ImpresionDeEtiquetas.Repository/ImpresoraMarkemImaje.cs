﻿using SaintGobain.ImpresionDeEtiquetas.Common;
using System.Collections.Generic;
using System.Threading;

namespace SaintGobain.ImpresionDeEtiquetas.Repository
{
    public class ImpresoraMarkemImaje
    {
        private readonly string hostname;
        private readonly int puertoTCP;
        private readonly int milisegundosTimeoutDeConexionTCP;
        private readonly PeticionesNGPCL peticionesNGPL;

        public ImpresoraMarkemImaje(string Hostname, int PuertoTCP, int MilisegundosTimeoutDeConexionTCP)
        {
            hostname = Hostname;
            puertoTCP = PuertoTCP;
            milisegundosTimeoutDeConexionTCP = MilisegundosTimeoutDeConexionTCP;
            peticionesNGPL = new PeticionesNGPCL();
        }
      
        public ImpresoraMarkemImaje(string Hostname, int PuertoTCP, int MilisegundosTimeoutDeConexionTCP, string ComandoObtenerEstadoImpresoraListaParaImprimir, string ComandoEjecutarImpresion, string RespuestaEsperadaEstadoImpresoraListaParaImprimir, string RespuestaEsperadaEjecutarImpresion, string RespuestaEsperadaSeleccionarJob, string RespuestaEsperadaActualizarJob)
        {
            hostname = Hostname;
            puertoTCP = PuertoTCP;
            milisegundosTimeoutDeConexionTCP = MilisegundosTimeoutDeConexionTCP;
            peticionesNGPL = new PeticionesNGPCL(ComandoObtenerEstadoImpresoraListaParaImprimir, ComandoEjecutarImpresion, RespuestaEsperadaEstadoImpresoraListaParaImprimir, RespuestaEsperadaEjecutarImpresion, RespuestaEsperadaSeleccionarJob, RespuestaEsperadaActualizarJob);
        }

        public bool ImprimirEtiqueta(EtiquetaAImprimir EtiquetaAImprimir)
        {
            string templateCSVConValores = TemplateManager.AsignarValoresATemplateMarkemImaje(EtiquetaAImprimir);
            bool impresionExitosa = ProcesarImpresion(templateCSVConValores);
            return impresionExitosa;
        }

        private bool ProcesarImpresion(string TemplateCSVConValores)
        {
            bool impresionExitosa = false;
            Logger.LogDebug("Seleccionando y actualizando Job de impresora.");
            bool templateSeleccionadoYActualizado = SeleccionarYActualizarJobEnImpresora(TemplateCSVConValores);
            if (templateSeleccionadoYActualizado == true)
            {
                //Logger.LogDebug("Evaluando si impresora está lista para imprimir.");
                //bool impresoraListaParaImprimir = EnviarPeticionAImpresora(PeticionesMarkemImaje.EstadoImpresoraListaParaImprimir);
                //if (impresoraListaParaImprimir == true)
                //{
                //    Logger.LogDebug("Enviando comando de impresión.");
                //    bool impresionEjecutada = EnviarPeticionAImpresora(PeticionesMarkemImaje.EjecutarImpresion);
                //    if (impresionEjecutada == true) impresionExitosa = true; Logger.LogDebug("Impresión exitosa.");
                //}
                impresionExitosa = true; 
                Logger.LogDebug("Impresión exitosa.");
            }
            return impresionExitosa;
        }

        private bool EnviarPeticionAImpresora(PeticionesMarkemImaje PeticionMarkemImaje, List<string> ListaParametrosAEnviar = null)
        {
            bool peticionEjecutadaExitosamente = false;
            (string comandoAEnviar, string respuestaEsperada) = ObtenerComandoYRespuestaEsperadaParaPeticion(PeticionMarkemImaje, ListaParametrosAEnviar);
            if (comandoAEnviar != "" && respuestaEsperada != "")
            {
                Logger.LogDebug($"ComandoAEnviar = {comandoAEnviar}");
                string respuestaRecibida = EnviarDataYRecibirRespuestaDeImpresora(comandoAEnviar).TrimStart('\u0002').TrimEnd('\u0003');
                if (respuestaEsperada == "" || respuestaEsperada == null)
                {
                    peticionEjecutadaExitosamente = true;
                }
                else
                {
                    Logger.LogDebug($"RespuestaRecibida = {respuestaRecibida}, RespuestaEsperada = {respuestaEsperada}");
                    if (respuestaRecibida == respuestaEsperada) peticionEjecutadaExitosamente = true;
                }
            }
            return peticionEjecutadaExitosamente;
        }

        private string EnviarDataYRecibirRespuestaDeImpresora(string ComandoAEnviar)
        {
            var tcpManager = new TCPManager(hostname, puertoTCP, milisegundosTimeoutDeConexionTCP);
            string respuestaRecibida = tcpManager.EnviarDataYRecibirRespuesta(ComandoAEnviar);
            tcpManager.CerrarConexion();
            return respuestaRecibida;
        }

        private (string ComandoAEnviar, string RespuestaEsperada) ObtenerComandoYRespuestaEsperadaParaPeticion(PeticionesMarkemImaje EstadoMarkemImaje, List<string> ListaParametrosAEnviar = null)
        {
            string comandoAEnviar = "";
            string respuestaEsperada = "";
            switch (EstadoMarkemImaje)
            {
                case PeticionesMarkemImaje.EstadoImpresoraListaParaImprimir:
                    comandoAEnviar = peticionesNGPL.ComandoObtenerEstadoImpresoraListaParaImprimir;
                    respuestaEsperada = peticionesNGPL.RespuestaEsperadaEstadoImpresoraListaParaImprimir;
                    break;
                case PeticionesMarkemImaje.EjecutarImpresion:
                    comandoAEnviar = peticionesNGPL.ComandoEjecutarImpresion;
                    respuestaEsperada = peticionesNGPL.RespuestaEsperadaEjecutarImpresion;
                    break;
                case PeticionesMarkemImaje.SeleccionarJob:
                    if(ListaParametrosAEnviar != null)
                    {
                        string nombreJobMarkemImaje = ListaParametrosAEnviar[0];
                        comandoAEnviar = GenerarComandoParaSeleccionarJobEnImpresora(nombreJobMarkemImaje);
                        respuestaEsperada = peticionesNGPL.RespuestaEsperadaSeleccionarJob;
                    }
                    else
                    {
                        Logger.LogWarn("No se recibió data adicional a enviar para la petición \"SeleccionarJob\".");
                    }
                    break;
                case PeticionesMarkemImaje.ActualizarJob:
                    if(ListaParametrosAEnviar != null)
                    {
                        comandoAEnviar = GenerarComandoParaActualizarJobEnImpresora(ListaParametrosAEnviar);
                        respuestaEsperada = peticionesNGPL.RespuestaEsperadaActualizarJob;
                    }
                    else
                    {
                        Logger.LogWarn("No se recibió data adicional a enviar para la petición \"ActualizarJob\".");
                    }
                    break;
                default:
                    break;
            }
            return (comandoAEnviar, respuestaEsperada);
        }

        private bool SeleccionarYActualizarJobEnImpresora(string TemplateCSV)
        {
            bool templateSeleccionadoYActualizado = false;
            List<string> listaParametros = TemplateManager.ObtenerParametrosDeTemplateCSV(TemplateCSV);
            if (listaParametros.Count > 0)
            {
                bool templateSeleccionado = EnviarPeticionAImpresora(PeticionesMarkemImaje.SeleccionarJob, listaParametros);
                if (templateSeleccionado == true)
                {
                    if (listaParametros.Count > 1)
                    {
                        Thread.Sleep(1000);
                        bool templateActualizado = EnviarPeticionAImpresora(PeticionesMarkemImaje.ActualizarJob, listaParametros);
                        if (templateActualizado == true) templateSeleccionadoYActualizado = true;
                    }
                    else
                    {
                        templateSeleccionadoYActualizado = true;
                    }
                }
            }
            return templateSeleccionadoYActualizado;
        }

        private string GenerarComandoParaSeleccionarJobEnImpresora(string NombreJob)
        {
            string comandoParaSeleccionarTemplate = "";
            if(NombreJob !=  null || NombreJob != "")
            {
                comandoParaSeleccionarTemplate = $"\u0002~JS0|{NombreJob}|0|\u0003";
                Logger.LogDebug($"Comando generado: {comandoParaSeleccionarTemplate}");
            }
            return comandoParaSeleccionarTemplate;
        }

        private string GenerarComandoParaActualizarJobEnImpresora(List<string> ListaParametrosAEnviar)
        {
            string comandoParaActualizarTemplate = "";
            if (ListaParametrosAEnviar.Count > 0)
            {
                for (int i = 0; i < ListaParametrosAEnviar.Count; i++)
                {
                    if (i == 0) 
                    {
                        comandoParaActualizarTemplate += ListaParametrosAEnviar[i] + "|0";
                    }
                    else
                    {
                        comandoParaActualizarTemplate += ListaParametrosAEnviar[i];
                    }
                    if (i < ListaParametrosAEnviar.Count -1) comandoParaActualizarTemplate += '|';
                }
                comandoParaActualizarTemplate = $"\u0002~JU|{comandoParaActualizarTemplate}\u0003";
                Logger.LogDebug($"Comando generado: {comandoParaActualizarTemplate}");
            }
            return comandoParaActualizarTemplate;
        }
    }
}
