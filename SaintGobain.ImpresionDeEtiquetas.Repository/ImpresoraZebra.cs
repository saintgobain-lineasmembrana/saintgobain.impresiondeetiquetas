﻿using SaintGobain.ImpresionDeEtiquetas.Common;
using System;

namespace SaintGobain.ImpresionDeEtiquetas.Repository
{
    public class ImpresoraZebra
    {
        private readonly string hostname;
        private readonly int puertoTCP;
        private readonly int milisegundosTimeoutDeConexionTCP;

        public ImpresoraZebra(string Hostname, int PuertoTCP, int MilisegundosTimeoutDeConexionTCP)
        {
            hostname = Hostname;
            puertoTCP = PuertoTCP;
            milisegundosTimeoutDeConexionTCP = MilisegundosTimeoutDeConexionTCP;
        }

        public bool ImprimirEtiqueta(EtiquetaAImprimir EtiquetaAImprimir)
        {
            string templateZPLConValores = TemplateManager.AsignarValoresATemplateZPL(EtiquetaAImprimir);
            var tcpManager = new TCPManager(hostname, puertoTCP, milisegundosTimeoutDeConexionTCP);

            bool dataEnviada = tcpManager.EnviarData(templateZPLConValores);
            tcpManager.CerrarConexion();

            return dataEnviada;
        }

        public bool ImprimirEtiqueta(string TextoZPL)
        {
            var tcpManager = new TCPManager(hostname, puertoTCP, milisegundosTimeoutDeConexionTCP);

            bool dataEnviada = tcpManager.EnviarData(TextoZPL);
            tcpManager.CerrarConexion();

            return dataEnviada;
        }
    }
}
