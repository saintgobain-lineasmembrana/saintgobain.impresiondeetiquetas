﻿using SaintGobain.ImpresionDeEtiquetas.Common;

namespace SaintGobain.ImpresionDeEtiquetas.Repository
{
    public class PeticionesNGPCL
    {
        public string ComandoObtenerEstadoImpresoraListaParaImprimir { get; set; }
        public string ComandoEjecutarImpresion { get; set; }
        public string RespuestaEsperadaEstadoImpresoraListaParaImprimir { get; set; }
        public string RespuestaEsperadaEjecutarImpresion { get; set; }
        public string RespuestaEsperadaSeleccionarJob { get; set; }
        public string RespuestaEsperadaActualizarJob { get; set; }

        public PeticionesNGPCL() 
        {
            AsignarValoresAPeticionesDesdeAppConfig();
        }

        public PeticionesNGPCL(string ComandoObtenerEstadoImpresoraListaParaImprimir, string ComandoEjecutarImpresion, string RespuestaEsperadaEstadoImpresoraListaParaImprimir, string RespuestaEsperadaEjecutarImpresion, string RespuestaEsperadaSeleccionarTemplate, string RespuestaEsperadaActualizarTemplate)
        {
            AsignarValoresAPeticiones(ComandoObtenerEstadoImpresoraListaParaImprimir, ComandoEjecutarImpresion, RespuestaEsperadaEstadoImpresoraListaParaImprimir, RespuestaEsperadaEjecutarImpresion, RespuestaEsperadaSeleccionarTemplate, RespuestaEsperadaActualizarTemplate);
        }

        private void AsignarValoresAPeticiones(string ComandoObtenerEstadoImpresoraListaParaImprimir, string ComandoEjecutarImpresion, string RespuestaEsperadaEstadoImpresoraListaParaImprimir, string RespuestaEsperadaEjecutarImpresion, string RespuestaEsperadaSeleccionarJob, string RespuestaEsperadaActualizarJob)
        {
            this.ComandoObtenerEstadoImpresoraListaParaImprimir = ComandoObtenerEstadoImpresoraListaParaImprimir;
            this.ComandoEjecutarImpresion = ComandoEjecutarImpresion;
            this.RespuestaEsperadaEstadoImpresoraListaParaImprimir = RespuestaEsperadaEstadoImpresoraListaParaImprimir;
            this.RespuestaEsperadaEjecutarImpresion = RespuestaEsperadaEjecutarImpresion;
            this.RespuestaEsperadaSeleccionarJob = RespuestaEsperadaSeleccionarJob;
            this.RespuestaEsperadaActualizarJob = RespuestaEsperadaActualizarJob;
        }

        private void AsignarValoresAPeticionesDesdeAppConfig()
        {
            this.ComandoObtenerEstadoImpresoraListaParaImprimir = ParameterManager.AppConfigParameters.GetString("ComandoObtenerEstadoImpresoraListaParaImprimir", "~PS|1|");
            this.ComandoEjecutarImpresion = ParameterManager.AppConfigParameters.GetString("ComandoEjecutarImpresion", "~PG|0|1|");
            this.RespuestaEsperadaEstadoImpresoraListaParaImprimir = ParameterManager.AppConfigParameters.GetString("RespuestaEsperadaEstadoImpresoraListaParaImprimir", "~PR0|0|");
            this.RespuestaEsperadaEjecutarImpresion = ParameterManager.AppConfigParameters.GetString("RespuestaEsperadaEjecutarImpresion", "~PG0|");
            this.RespuestaEsperadaSeleccionarJob = ParameterManager.AppConfigParameters.GetString("RespuestaEsperadaSeleccionarJob", "~JS0|");
            this.RespuestaEsperadaActualizarJob = ParameterManager.AppConfigParameters.GetString("RespuestaEsperadaActualizarJob", "~JU0|");
        }
    }
}
