﻿using System;
using System.Threading;
using SaintGobain.ImpresionDeEtiquetas.Common;
using SaintGobain.ImpresionDeEtiquetas.Repository;


namespace SaintGobain.ImpresionDeEtiquetas.ServicioDeImpresion
{
    public class Principal
    {
        private readonly int segundosEsperaImprimirEtiquetasNoProcesadas;
        private readonly int milisegundosTimeoutDeConexionTCP;
        private readonly Impresion impresion;
        private readonly Thread threadImprimirEtiquetasNoProcesadas;

        public Principal()
        {
            milisegundosTimeoutDeConexionTCP = ParameterManager.AppConfigParameters.GetInt("MilisegundosTimeoutDeConexionTCP", 5000);
            segundosEsperaImprimirEtiquetasNoProcesadas = ParameterManager.AppConfigParameters.GetInt("SegundosEsperaImprimirEtiquetasNoProcesadas", 10);
            impresion = new Impresion(milisegundosTimeoutDeConexionTCP);
            threadImprimirEtiquetasNoProcesadas = new Thread(new ThreadStart((CicloImprimirEtiquetasNoProcesadas)));
        }

        private void CicloImprimirEtiquetasNoProcesadas()
        {
            while (true)
            {
                try
                {
                    impresion.ImprimirEtiquetasNoProcesadasPorHilos();
                }
                catch (Exception e)
                {
                    Logger.LogError($"Error al imprimir etiquetas:\n{e}");
                }
                Thread.Sleep(TimeSpan.FromSeconds(segundosEsperaImprimirEtiquetasNoProcesadas));
            }
        }

        public void IniciarServicio()
        {
            threadImprimirEtiquetasNoProcesadas.Start();
        }
        
        public void DetenerServicio()
        {
            threadImprimirEtiquetasNoProcesadas.Abort();
            while (threadImprimirEtiquetasNoProcesadas.IsAlive == true)
            {
                Thread.Sleep(1000);
            }
        }
    }
}
