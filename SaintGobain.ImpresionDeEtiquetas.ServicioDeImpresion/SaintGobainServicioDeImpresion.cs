﻿using SaintGobain.ImpresionDeEtiquetas.Common;
using System.ServiceProcess;

namespace SaintGobain.ImpresionDeEtiquetas.ServicioDeImpresion
{
    public partial class SaintGobainServicioDeImpresion : ServiceBase
    {
        private Principal principal;

        public SaintGobainServicioDeImpresion()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            Logger.LogTitle("Saint Gobain - Servicio de Impresión iniciado.");
            principal = new Principal();
            principal.IniciarServicio();
        }

        protected override void OnStop()
        {
            principal.DetenerServicio();
            Logger.LogTitle("Saint Gobain - Servicio de Impresión detenido.");
        }
    }
}
