﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration.Install;
using System.Linq;
using System.Threading.Tasks;

namespace SaintGobain.ImpresionDeEtiquetas.ServicioDeImpresion
{
    [RunInstaller(true)]
    public partial class SaintGobainServicioDeImpresionInstaller : System.Configuration.Install.Installer
    {
        public SaintGobainServicioDeImpresionInstaller()
        {
            InitializeComponent();
        }

        private void serviceInstaller1_AfterInstall(object sender, InstallEventArgs e)
        {

        }

        private void serviceProcessInstaller1_AfterInstall(object sender, InstallEventArgs e)
        {

        }
    }
}
