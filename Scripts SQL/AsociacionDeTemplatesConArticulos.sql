---- Asociación de Templates con Artículos ----
Use [SGPv2_SG]

delete LPMEtiquetaArticulo

Dbcc CHECKIDENT ('LPMEtiquetaArticulo', RESEED, 0);
Go

Declare @i int;
Declare @j int;
Declare @lastId int;
Set @i = 1;
Set @j = 1;
Set @lastId = (Select top 1 Id from Articulo order by id desc);


While(@i <= @lastId)
Begin
	If ((Select Count(id) from Articulo where id = @i) = 1)
	Begin 
		Insert into LPMEtiquetaArticulo (ArticuloId, CantidadEtiquetas, TemplateEtiquetaId) values (@i, 1, @j);
		Set @j = @j +1;
		Insert into LPMEtiquetaArticulo (ArticuloId, CantidadEtiquetas, TemplateEtiquetaId) values (@i, 1, @j);
		Set @j = @j +1;
	End
	Set @i = @i + 1;
End
----