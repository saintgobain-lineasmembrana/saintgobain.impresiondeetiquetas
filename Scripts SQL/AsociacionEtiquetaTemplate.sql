--Revisar artículos a actualizar
use [SGPv2_SG]
select * from articulo a
where
a.Nombre = 'M17MAAAGMGR40KMG' or
a.Nombre = 'M17MAAAGMRO40KMG' or
a.Nombre = 'M17MAAAMMNC03MSA' or
a.Nombre = 'M17MAAAMXNC25KBM' or
a.Nombre = 'M17MAAAMXNC35KBM' or
a.Nombre = 'M17MAAAMXNC40KBM' or
a.Nombre = 'MAAACSA030KSA' or
a.Nombre = 'MAAACSA040KSA' or
a.Nombre = 'MAAACSAO30KSA' or
a.Nombre = 'MAAAGNPR04MSA' or
a.Nombre = 'MAAAMGR04MMMT' or
a.Nombre = 'MAAAMMTR40KTE' or
a.Nombre = 'MAAAMXAO25KSA' or
a.Nombre = 'MAAAMXAO30KSA' or
a.Nombre = 'MAAAMXAO35KSA' or
a.Nombre = 'MAAAMXAO40KSA' or
a.Nombre = 'MAAAMXAO40KTE' or
a.Nombre = 'MAAAMXAO40PSA' or
a.Nombre = 'MAAAMXAOKTE' or
a.Nombre = 'MAAAMXTR15KMF' or
a.Nombre = 'MAAAMXTR25KMF' or
a.Nombre = 'MAAANR200EKMT' or
a.Nombre = 'PTAUPO0110BLMF' or
a.Nombre = 'PTROBL04MMGEMG' or
a.Nombre = 'PTROGR40KGREMG' or
a.Nombre = 'PTROPO0110ROMF' or
a.Nombre = 'PTROPO04MMGEMG' or
a.Nombre = 'PTRORO04MMPOMG' or
a.Nombre = 'PTRORO40KGREMG' or
a.Nombre = 'PTROTR14KGPOCL' or
a.Nombre = 'PTROTR14KGPOMB' or
a.Nombre = 'PTROTR14KGPOME' or
a.Nombre = 'PTROTR14KGPOMG' or
a.Nombre = 'PTROTR20KGPOCL' or
a.Nombre = 'PTROTR20KGPOMB' or
a.Nombre = 'PTROTR35KGPOSI' or
a.Nombre = 'PTROTR35KGPOSK' or
a.Nombre = 'PTROTR35KGPOWBPA' or
a.Nombre = 'PTROTR40KGPOSK' or
a.Nombre = 'PTROUR25KGPOMG' or
a.Nombre = 'SECA2803' or
a.Nombre = 'SECA2805' or
a.Nombre = 'SECA2806' or
a.Nombre = 'SECA2807' or
a.Nombre = 'SECA2808' or
a.Nombre = 'SECA2809' or
a.Nombre = 'SECA2810' 
order by a.Nombre


---Revisar asociación de etiquetas LPMEtiquetaArticulo
use [SGPv2_SG]
select a.id, ea.ArticuloId, a.Nombre, ea.TemplateEtiquetaId from LPMEtiquetaArticulo ea
inner join articulo a on a.id = ea.ArticuloId
where
a.Nombre = 'M17MAAAGMGR40KMG' or
a.Nombre = 'M17MAAAGMRO40KMG' or
a.Nombre = 'M17MAAAMMNC03MSA' or
a.Nombre = 'M17MAAAMXNC25KBM' or
a.Nombre = 'M17MAAAMXNC35KBM' or
a.Nombre = 'M17MAAAMXNC40KBM' or
a.Nombre = 'MAAACSA030KSA' or
a.Nombre = 'MAAACSA040KSA' or
a.Nombre = 'MAAACSAO30KSA' or
a.Nombre = 'MAAAGNPR04MSA' or
a.Nombre = 'MAAAMGR04MMMT' or
a.Nombre = 'MAAAMMTR40KTE' or
a.Nombre = 'MAAAMXAO25KSA' or
a.Nombre = 'MAAAMXAO30KSA' or
a.Nombre = 'MAAAMXAO35KSA' or
a.Nombre = 'MAAAMXAO40KSA' or
a.Nombre = 'MAAAMXAO40KTE' or
a.Nombre = 'MAAAMXAO40PSA' or
a.Nombre = 'MAAAMXAOKTE' or
a.Nombre = 'MAAAMXTR15KMF' or
a.Nombre = 'MAAAMXTR25KMF' or
a.Nombre = 'MAAANR200EKMT' or
a.Nombre = 'PTAUPO0110BLMF' or
a.Nombre = 'PTROBL04MMGEMG' or
a.Nombre = 'PTROGR40KGREMG' or
a.Nombre = 'PTROPO0110ROMF' or
a.Nombre = 'PTROPO04MMGEMG' or
a.Nombre = 'PTRORO04MMPOMG' or
a.Nombre = 'PTRORO40KGREMG' or
a.Nombre = 'PTROTR14KGPOCL' or
a.Nombre = 'PTROTR14KGPOMB' or
a.Nombre = 'PTROTR14KGPOME' or
a.Nombre = 'PTROTR14KGPOMG' or
a.Nombre = 'PTROTR20KGPOCL' or
a.Nombre = 'PTROTR20KGPOMB' or
a.Nombre = 'PTROTR35KGPOSI' or
a.Nombre = 'PTROTR35KGPOSK' or
a.Nombre = 'PTROTR35KGPOWBPA' or
a.Nombre = 'PTROTR40KGPOSK' or
a.Nombre = 'PTROUR25KGPOMG' or
a.Nombre = 'SECA2803' or
a.Nombre = 'SECA2805' or
a.Nombre = 'SECA2806' or
a.Nombre = 'SECA2807' or
a.Nombre = 'SECA2808' or
a.Nombre = 'SECA2809' or
a.Nombre = 'SECA2810' 
order by a.Nombre


---Actualización de tabla LPMEtiquetaArticulo con TemplateEtiquetaId NUEVO
use [SGPv2_SG]
update LPMEtiquetaArticulo set TemplateEtiquetaId = 6 where ArticuloId = 240;
update LPMEtiquetaArticulo set TemplateEtiquetaId = 6 where ArticuloId = 241;
update LPMEtiquetaArticulo set TemplateEtiquetaId = 8 where ArticuloId = 242;
update LPMEtiquetaArticulo set TemplateEtiquetaId = 7 where ArticuloId = 31;
update LPMEtiquetaArticulo set TemplateEtiquetaId = 7 where ArticuloId = 37;
update LPMEtiquetaArticulo set TemplateEtiquetaId = 7 where ArticuloId = 41;
update LPMEtiquetaArticulo set TemplateEtiquetaId = 7 where ArticuloId = 243;
update LPMEtiquetaArticulo set TemplateEtiquetaId = 6 where ArticuloId = 251;
update LPMEtiquetaArticulo set TemplateEtiquetaId = 6 where ArticuloId = 252;
update LPMEtiquetaArticulo set TemplateEtiquetaId = 6 where ArticuloId = 244;
update LPMEtiquetaArticulo set TemplateEtiquetaId = 8 where ArticuloId = 63;
update LPMEtiquetaArticulo set TemplateEtiquetaId = 8 where ArticuloId = 73;
update LPMEtiquetaArticulo set TemplateEtiquetaId = 8 where ArticuloId = 75;
update LPMEtiquetaArticulo set TemplateEtiquetaId = 8 where ArticuloId = 92;
update LPMEtiquetaArticulo set TemplateEtiquetaId = 8 where ArticuloId = 93;
update LPMEtiquetaArticulo set TemplateEtiquetaId = 8 where ArticuloId = 94;
update LPMEtiquetaArticulo set TemplateEtiquetaId = 8 where ArticuloId = 96;
update LPMEtiquetaArticulo set TemplateEtiquetaId = 8 where ArticuloId = 97;
update LPMEtiquetaArticulo set TemplateEtiquetaId = 8 where ArticuloId = 246;
update LPMEtiquetaArticulo set TemplateEtiquetaId = 8 where ArticuloId = 98;
update LPMEtiquetaArticulo set TemplateEtiquetaId = 8 where ArticuloId = 99;
update LPMEtiquetaArticulo set TemplateEtiquetaId = 8 where ArticuloId = 100;
update LPMEtiquetaArticulo set TemplateEtiquetaId = 8 where ArticuloId = 102;
update LPMEtiquetaArticulo set TemplateEtiquetaId = 6 where ArticuloId = 256;
update LPMEtiquetaArticulo set TemplateEtiquetaId = 6 where ArticuloId = 107;
update LPMEtiquetaArticulo set TemplateEtiquetaId = 6 where ArticuloId = 127;
update LPMEtiquetaArticulo set TemplateEtiquetaId = 6 where ArticuloId = 144;
update LPMEtiquetaArticulo set TemplateEtiquetaId = 7 where ArticuloId = 147;
update LPMEtiquetaArticulo set TemplateEtiquetaId = 6 where ArticuloId = 157;
update LPMEtiquetaArticulo set TemplateEtiquetaId = 6 where ArticuloId = 161;
update LPMEtiquetaArticulo set TemplateEtiquetaId = 8 where ArticuloId = 163;
update LPMEtiquetaArticulo set TemplateEtiquetaId = 8 where ArticuloId = 164;
update LPMEtiquetaArticulo set TemplateEtiquetaId = 8 where ArticuloId = 165;
update LPMEtiquetaArticulo set TemplateEtiquetaId = 8 where ArticuloId = 166;
update LPMEtiquetaArticulo set TemplateEtiquetaId = 6 where ArticuloId = 258;
update LPMEtiquetaArticulo set TemplateEtiquetaId = 6 where ArticuloId = 259;
update LPMEtiquetaArticulo set TemplateEtiquetaId = 8 where ArticuloId = 193;
update LPMEtiquetaArticulo set TemplateEtiquetaId = 6 where ArticuloId = 261;
update LPMEtiquetaArticulo set TemplateEtiquetaId = 6 where ArticuloId = 197;
update LPMEtiquetaArticulo set TemplateEtiquetaId = 6 where ArticuloId = 260;
update LPMEtiquetaArticulo set TemplateEtiquetaId = 6 where ArticuloId = 247;
update LPMEtiquetaArticulo set TemplateEtiquetaId = 4 where ArticuloId = 219;
update LPMEtiquetaArticulo set TemplateEtiquetaId = 6 where ArticuloId = 220;
update LPMEtiquetaArticulo set TemplateEtiquetaId = 6 where ArticuloId = 225;
update LPMEtiquetaArticulo set TemplateEtiquetaId = 6 where ArticuloId = 221;
update LPMEtiquetaArticulo set TemplateEtiquetaId = 6 where ArticuloId = 222;
update LPMEtiquetaArticulo set TemplateEtiquetaId = 6 where ArticuloId = 223;
update LPMEtiquetaArticulo set TemplateEtiquetaId = 6 where ArticuloId = 224;


---Actualización de tabla LPMEtiquetaArticulo con TemplateEtiquetaId VIEJO
use [SGPv2_SG]
update LPMEtiquetaArticulo set TemplateEtiquetaId = 4 where ArticuloId = 240;
update LPMEtiquetaArticulo set TemplateEtiquetaId = 4 where ArticuloId = 241;
update LPMEtiquetaArticulo set TemplateEtiquetaId = 1 where ArticuloId = 242;
update LPMEtiquetaArticulo set TemplateEtiquetaId = 1 where ArticuloId = 31;
update LPMEtiquetaArticulo set TemplateEtiquetaId = 3 where ArticuloId = 37;
update LPMEtiquetaArticulo set TemplateEtiquetaId = 3 where ArticuloId = 41;
update LPMEtiquetaArticulo set TemplateEtiquetaId = 3 where ArticuloId = 243;
update LPMEtiquetaArticulo set TemplateEtiquetaId = 2 where ArticuloId = 251;
update LPMEtiquetaArticulo set TemplateEtiquetaId = 4 where ArticuloId = 252;
update LPMEtiquetaArticulo set TemplateEtiquetaId = 4 where ArticuloId = 244;
update LPMEtiquetaArticulo set TemplateEtiquetaId = 1 where ArticuloId = 63;
update LPMEtiquetaArticulo set TemplateEtiquetaId = 1 where ArticuloId = 73;
update LPMEtiquetaArticulo set TemplateEtiquetaId = 1 where ArticuloId = 75;
update LPMEtiquetaArticulo set TemplateEtiquetaId = 1 where ArticuloId = 92;
update LPMEtiquetaArticulo set TemplateEtiquetaId = 1 where ArticuloId = 93;
update LPMEtiquetaArticulo set TemplateEtiquetaId = 1 where ArticuloId = 94;
update LPMEtiquetaArticulo set TemplateEtiquetaId = 1 where ArticuloId = 96;
update LPMEtiquetaArticulo set TemplateEtiquetaId = 1 where ArticuloId = 97;
update LPMEtiquetaArticulo set TemplateEtiquetaId = 1 where ArticuloId = 246;
update LPMEtiquetaArticulo set TemplateEtiquetaId = 1 where ArticuloId = 98;
update LPMEtiquetaArticulo set TemplateEtiquetaId = 1 where ArticuloId = 99;
update LPMEtiquetaArticulo set TemplateEtiquetaId = 1 where ArticuloId = 100;
update LPMEtiquetaArticulo set TemplateEtiquetaId = 1 where ArticuloId = 102;
update LPMEtiquetaArticulo set TemplateEtiquetaId = 1 where ArticuloId = 256;
update LPMEtiquetaArticulo set TemplateEtiquetaId = 1 where ArticuloId = 107;
update LPMEtiquetaArticulo set TemplateEtiquetaId = 1 where ArticuloId = 127;
update LPMEtiquetaArticulo set TemplateEtiquetaId = 1 where ArticuloId = 144;
update LPMEtiquetaArticulo set TemplateEtiquetaId = 3 where ArticuloId = 147;
update LPMEtiquetaArticulo set TemplateEtiquetaId = 1 where ArticuloId = 157;
update LPMEtiquetaArticulo set TemplateEtiquetaId = 1 where ArticuloId = 161;
update LPMEtiquetaArticulo set TemplateEtiquetaId = 1 where ArticuloId = 163;
update LPMEtiquetaArticulo set TemplateEtiquetaId = 1 where ArticuloId = 164;
update LPMEtiquetaArticulo set TemplateEtiquetaId = 1 where ArticuloId = 165;
update LPMEtiquetaArticulo set TemplateEtiquetaId = 1 where ArticuloId = 166;
update LPMEtiquetaArticulo set TemplateEtiquetaId = 1 where ArticuloId = 258;
update LPMEtiquetaArticulo set TemplateEtiquetaId = 1 where ArticuloId = 259;
update LPMEtiquetaArticulo set TemplateEtiquetaId = 1 where ArticuloId = 193;
update LPMEtiquetaArticulo set TemplateEtiquetaId = 4 where ArticuloId = 261;
update LPMEtiquetaArticulo set TemplateEtiquetaId = 1 where ArticuloId = 197;
update LPMEtiquetaArticulo set TemplateEtiquetaId = 4 where ArticuloId = 260;
update LPMEtiquetaArticulo set TemplateEtiquetaId = 3 where ArticuloId = 247;
update LPMEtiquetaArticulo set TemplateEtiquetaId = 1 where ArticuloId = 219;
update LPMEtiquetaArticulo set TemplateEtiquetaId = 1 where ArticuloId = 220;
update LPMEtiquetaArticulo set TemplateEtiquetaId = 2 where ArticuloId = 225;
update LPMEtiquetaArticulo set TemplateEtiquetaId = 1 where ArticuloId = 221;
update LPMEtiquetaArticulo set TemplateEtiquetaId = 1 where ArticuloId = 222;
update LPMEtiquetaArticulo set TemplateEtiquetaId = 1 where ArticuloId = 223;
update LPMEtiquetaArticulo set TemplateEtiquetaId = 1 where ArticuloId = 224;
