USE [SGPv2_SG]
GO

/****** Object:  StoredProcedure [dbo].[GetEtiquetasAImprimir]    Script Date: 01/11/2021 12:46:24 p.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Trikom - Ricardo Blanco Dias
-- Create date: 2021-09-22
-- Description:	SP para obtener el listado de mensajes de rollo a imprimir (con informaci�n completa tra�da de las tablas "Articulo" y "UnidadProductiva").
-- =============================================
CREATE PROCEDURE [dbo].[GenerarTemplatesCSVParaImpresoraMarkemImaje]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE te
	SET te.Template = CONCAT(a.Nombre, ',TEXT_13,TEXT_Lote,TEXT_OP')
	FROM LPMTemplateEtiqueta AS te
	INNER JOIN LPMEtiquetaArticulo AS ta ON te.Id = ta.TemplateEtiquetaId
	INNER JOIN Articulo as a ON ta.ArticuloId = a.Id
	WHERE te.Template = '{LoteNum},{Fecha},{Hora},{ArticuloCod},{CodOrden},{Articulo},{RolloNum},{Esp}';
END
GO


