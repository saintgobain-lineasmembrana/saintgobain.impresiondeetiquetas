USE [SGPv2_SG]
GO

/****** Object:  StoredProcedure [dbo].[GetEtiquetasAImprimir]    Script Date: 11/4/2021 3:03:03 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Trikom - Ricardo Blanco Dias
-- Create date: 2021-09-22
-- Description:	SP para obtener el listado de mensajes de rollo a imprimir (con informaci�n completa tra�da de las tablas "Articulo" y "UnidadProductiva").
-- =============================================
CREATE PROCEDURE [dbo].[GetEtiquetasAImprimir]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT sgmr.Id as MensajeRolloId, sgmr.Fecha, sgmr.CodigoOrden, sgmr.Longitud, sgmr.Peso, sgmr.Espesor, sgmr.RolloNumero, sgmr.LoteNumero, a.Codigo as ArticuloCodigo, a.Nombre as ArticuloNombre, a.Descripcion as ArticuloDescripcion, up.Nombre AS UnidadProductivaNombre, i.Nombre AS ImpresoraNombre, i.Id AS ImpresoraId, ti.id AS TipoImpresoraId, i.Hostname AS ImpresoraHostname, i.Puerto AS ImpresoraPuerto, te.TemplateZPL AS TemplateZPL, te.TemplateMarkemImaje AS TemplateMarkemImaje, sgmr.EstadoEtiquetaId FROM SG_MensajeRollo sgmr 
		INNER JOIN Articulo a ON a.id = sgmr.ArticuloId 
		INNER JOIN UnidadProductiva up ON up.Id = sgmr.UnidadProductivaId
		INNER JOIN LPMImpresoraEquipo ie ON ie.UnidadProductivaId = up.Id--sgmr.UnidadProductivaId 
		INNER JOIN LPMEtiquetaArticulo ea ON ea.ArticuloId = a.id--sgmr.ArticuloId 
		INNER JOIN LPMTemplateEtiqueta te ON te.id  = ea.TemplateEtiquetaId	
		INNER JOIN LPMImpresora i ON i.Id = ie.ImpresoraId ---AND i.Id = te.ImpresoraId
		INNER JOIN LPMTipoImpresora ti ON ti.id = i.TipoImpresoraId
	WHERE sgmr.EstadoEtiquetaId = 1;
END
GO


